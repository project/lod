<?php

namespace Drupal\lod;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\lod\Annotation\LodNormalizer;

/**
 * Class LodNormalizerManager.
 */
class LodNormalizerManager extends DefaultPluginManager implements LodNormalizerManagerInterface {

  /**
   * Plugins grouped by format.
   *
   * @var array
   *   Plugins grouped by format.
   */
  protected $pluginsByFormat = [];

  /**
   * Constructs a LodNormalizerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/LodNormalizer',
      $namespaces,
      $module_handler,
      LodNormalizerPluginInterface::class,
      LodNormalizer::class
    );
    $this->alterInfo('lod_normalizer_info');
    $this->setCacheBackend($cache_backend, 'lod_normalizer_info_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionsByFormat($format) {
    if (!isset($this->pluginsByFormat[$format])) {
      $plugins = $this->getDefinitions();

      // Filter plugins on their properties.
      $plugins = array_filter($plugins, function ($plugin) use ($format) {
        return $plugin['format'] === $format;
      });

      // Order plugins on weight.
      $order = array_column($plugins, 'weight');
      array_multisort($order, SORT_ASC, $plugins);

      $this->pluginsByFormat[$format] = $plugins;
    }

    return $this->pluginsByFormat[$format];
  }

}
