<?php

namespace Drupal\lod\Plugin\LodNormalizer;

use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Value\NormalizerContext;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:view",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\views\ViewExecutable",
 *   weight = 1000,
 * )
 */
class View extends BasePlugin {

  /**
   * Url generator service.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SerializerInterface $serializer, UrlGeneratorInterface $url_generator) {
    $this->urlGenerator = $url_generator;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $configuration['serializer'],
      $container->get('url_generator')
    );
  }

  /**
   * Normalize a view.
   *
   * @param array|\Drupal\views\ViewExecutable $view
   *   The view to normalize.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Normalizer context.
   *
   * @return array
   *   Normalized array.
   */
  public function normalize($view, NormalizerContext $context) {
    /* @var $view \Drupal\views\ViewExecutable */
    $rows = [];
    foreach ($view->result as $row_index => $row) {
      $view->row_index = $row_index;
      $rows[] = $view->rowPlugin->render($row);
    }
    unset($view->row_index);

    $normalized = $this->getHydraTags($view);
    $normalized['member'] = $this->serializer->normalize($rows, JsonLdEncoder::FORMAT, (array) $context);

    return $normalized;
  }

  /**
   * Get the Hydra (paging) tags from the view.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view to get the tags for.
   *
   * @return array
   *   The normalized Hydra tags.
   */
  protected function getHydraTags(ViewExecutable $view) {
    $pager = $view->getPager();

    $currentPage = (int) $pager->getCurrentPage();

    $exposedInput = $view->getExposedInput();

    $normalized = [
      '@id' => $this->getUriForPage($currentPage, $exposedInput),
      '@type' => 'hydra:PartialCollectionView',
      '@context' => [
        'hydra' => 'http://www.w3.org/ns/hydra/core#',
        'firstPage' => 'hydra:first',
        'previousPage' => 'hydra:previous',
        'nextPage' => 'hydra:next',
        'lastPage' => 'hydra:last',
        'member' => 'hydra:member',
      ],
    ];

    if ($pager->getPluginId() !== 'full' && $pager->getPluginId() !== 'mini') {
      return $normalized;
    }

    $normalized['firstPage'] = $this->getUriForPage(0, $exposedInput);

    if ($currentPage > 0) {
      $normalized['previousPage'] = $this->getUriForPage($currentPage - 1, $exposedInput);
    }

    if ($pager->hasMoreRecords()) {
      $normalized['nextPage'] = $this->getUriForPage($currentPage + 1, $exposedInput);
    }

    $totalItems = $pager->getTotalItems();
    // getTotalItems does not return the correct number of items, but the
    // correct method below is not (easily) mockable.
    // $totalItems = $view->getQuery()->countQuery()->execute()->fetchField();
    // Using floor as page numbers start at zero.
    $total = floor($totalItems / $pager->getItemsPerPage());
    $normalized['lastPage'] = $this->getUriForPage($total, $exposedInput);

    return $normalized;
  }

  /**
   * Get the current uri with the given page number.
   *
   * @param int $page
   *   The page number.
   * @param array $exposed_input
   *   The exposed input.
   *
   * @return string
   *   The uri.
   */
  protected function getUriForPage($page = 0, array $exposed_input = []) {
    $params = !$page ? [] : [
      'page' => $page,
    ];
    $params += $exposed_input;

    return (string) $this->urlGenerator->generateFromRoute(
      '<current>',
      $params,
      ['absolute' => TRUE]
    );
  }

}
