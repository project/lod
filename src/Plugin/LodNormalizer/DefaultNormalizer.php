<?php

namespace Drupal\lod\Plugin\LodNormalizer;

use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Value\NormalizerContext;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:default",
 *   format = "json_ld",
 *   weight = 2000,
 * )
 */
class DefaultNormalizer extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function normalize($object, NormalizerContext $context) {
    if ($object instanceof \Traversable) {
      $data = [];
      foreach ($object as $item) {
        $data[] = $this->serializer->normalize($item, JsonLdEncoder::FORMAT, (array) $context);
      }

      return $data;
    }

    // No json_ld plugins found, fallback to format independent normalizers.
    return $this->serializer->normalize($object, NULL, (array) $context);
  }

}
