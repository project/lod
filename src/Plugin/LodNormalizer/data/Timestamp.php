<?php

namespace Drupal\lod\Plugin\LodNormalizer\data;

use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:timestamp",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\TypedData\Plugin\DataType\Timestamp",
 *   weight = 1000,
 * )
 */
class Timestamp extends BasePlugin {

  /**
   * Convert timestamp to ISO 8601 format.
   *
   * {@inheritdoc}
   */
  public function normalize($object, NormalizerContext $context) {
    $date = \DateTime::createFromFormat('U', $object->getCastedValue(), new \DateTimeZone('UTC'));
    return $date->format('c');
  }

}
