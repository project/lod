<?php

namespace Drupal\lod\Plugin\LodNormalizer;

use Drupal\Core\Plugin\PluginBase;
use Drupal\lod\LodNormalizerPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class BaseNormalizer.
 */
abstract class BasePlugin extends PluginBase implements LodNormalizerPluginInterface {

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SerializerInterface $serializer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $configuration['serializer']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($object) {
    // By default we support all objects, can be overridden to test for specific
    // objects. E.g check entity bundle.
    return TRUE;
  }

}
