<?php

namespace Drupal\lod\Plugin\LodNormalizer\entity;

use Drupal\lod\Value\NormalizerContext;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:term",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\taxonomy\TermInterface",
 *   weight = 990,
 * )
 */
class Term extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  protected function preNormalize(NormalizerContext $context) {
    parent::preNormalize($context);

    $context->addExcludedField('tid');
    $context->addExcludedField('path');
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($term, NormalizerContext $context) {
    $normalized = parent::normalize($term, $context);

    /* @var \Drupal\taxonomy\TermInterface $term */
    $normalized['vocabulary'] = $term->bundle();

    if (!isset($normalized['parent'])) {
      $normalized['parent'] = [];
    }

    $normalized['url'] = $term->toUrl('canonical', ['absolute' => TRUE])->toString();
    $normalized['page'] = $term->toUrl('canonical', [
      'absolute' => TRUE,
      'path_processing' => FALSE,
    ])->toString();

    return $normalized;
  }

}
