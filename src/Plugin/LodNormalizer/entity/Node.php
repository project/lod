<?php

namespace Drupal\lod\Plugin\LodNormalizer\entity;

use Drupal\lod\Value\NormalizerContext;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:node",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\node\NodeInterface",
 *   weight = 1000,
 * )
 */
class Node extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  protected function preNormalize(NormalizerContext $context) {
    parent::preNormalize($context);

    $context->addExcludedField('nid');
    $context->addExcludedField('path');
    $context->addFieldTranslation('title', 'name');
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($node, NormalizerContext $context) {
    $normalized = parent::normalize($node, $context);

    /* @var \Drupal\node\NodeInterface $node */
    $normalized['url'] = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
    $normalized['page'] = $node->toUrl('canonical', [
      'absolute' => TRUE,
      'path_processing' => FALSE,
    ])->toString();

    return $normalized;
  }

}
