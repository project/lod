<?php

namespace Drupal\lod\Plugin\LodNormalizer\entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\TypedDataInternalPropertiesHelper;
use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\FieldItemListDetails;
use Drupal\lod\Value\NormalizerContext;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:entity",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Entity\ContentEntityInterface",
 *   weight = 1000,
 * )
 */
class ContentEntity extends BasePlugin {

  /**
   * Prepare the context for normalization.
   *
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Context options for the normalizer.
   */
  protected function preNormalize(NormalizerContext $context) {
    $context->addExcludedField('vid');
    $context->addExcludedField('revision_timestamp');
    $context->addExcludedField('revision_uid');
    $context->addExcludedField('revision_log');
    $context->addExcludedField('revision_translation_affected');
    $context->addExcludedField('uid');
    $context->addExcludedField('langcode');
    $context->addExcludedField('default_langcode');
    $context->addExcludedField('status');
    $context->addExcludedField('content_translation_source');
    $context->addExcludedField('content_translation_outdated');
    $context->addExcludedField('content_translation_uid');
    $context->addExcludedField('content_translation_created');
    $context->addExcludedField('field_metatags');
    $context->addExcludedField('metatag');
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, NormalizerContext $context) {
    $this->preNormalize($context);

    $normalized = [];
    foreach ($this->getFieldItems($entity, $context) as $field_name => $field) {
      $normalized[$field_name] = $this->normalizeField($field, $context);
    }

    return $this->postNormalize($normalized, $context);
  }

  /**
   * Cleanup after the object was normalized.
   *
   * @param array $normalized
   *   The normalized object as array.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Context options for the normalizer.
   *
   * @return array
   *   The normalized object as array.
   */
  protected function postNormalize(array $normalized, NormalizerContext $context) {
    $normalized = $this->mergeChildFields($normalized, $context);
    $normalized = $this->renameFields($normalized, $context);

    return $normalized;
  }

  /**
   * Get the field items to normalize.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to normalize.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Context options for the normalizer.
   *
   * @return \Drupal\Core\TypedData\TypedDataInterface[]
   *   The field items to normalize.
   */
  protected function getFieldItems(ContentEntityInterface $entity, NormalizerContext $context) {
    $field_items = TypedDataInternalPropertiesHelper::getNonInternalProperties($entity->getTypedData());

    // If the fields to use were specified, only output those field values.
    if ($included_fields = $context->getIncludedFields()) {
      return array_intersect_key($field_items, array_flip($included_fields));
    }

    // Remove excluded fields.
    $field_items = array_diff_key($field_items, array_flip($context->getExcludedFields()));

    return $field_items;
  }

  /**
   * Normalize a single field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field to normalize.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Normalizer context.
   *
   * @return array
   *   The field as a set of arrays.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function normalizeField(FieldItemListInterface $field, NormalizerContext $context) {
    $normalized = $this->serializer->normalize($field, JsonLdEncoder::FORMAT, (array) $context);
    $fieldDetails = new FieldItemListDetails($field);

    // Check if we can reduce the normalized array depth.
    if (!\is_array($normalized)
      || !isset($normalized[0])
      || $fieldDetails->isMultiValue()
    ) {
      return $normalized;
    }

    $value = $normalized[0];
    if (!\is_array($value)) {
      return $value;
    }

    unset($normalized[0]);
    return NestedArray::mergeDeep($normalized, $value);
  }

  /**
   * Merge child fields into the parent.
   *
   * @param array $normalized
   *   The normalized entity.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Normalizer context.
   *
   * @return array
   *   Normalize array with renamed keys.
   */
  protected function mergeChildFields(array $normalized, NormalizerContext $context) {
    foreach ($normalized as $name => $field) {
      if (!empty($field['merge_parent'])) {
        unset($field['merge_parent'], $normalized[$name]);
        $normalized = NestedArray::mergeDeep($normalized, $field);
      }
    }

    return $normalized;
  }

  /**
   * Rename the child fields according to the normalizer context.
   *
   * @param array $normalized
   *   The normalized entity.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Normalizer context.
   *
   * @return array
   *   Normalize array with renamed keys.
   */
  protected function renameFields(array $normalized, NormalizerContext $context) {
    foreach ($context->getFieldTranslations() as $original_name => $new_name) {
      if (array_key_exists($original_name, $normalized)) {
        $normalized[$new_name] = $normalized[$original_name];
        unset($normalized[$original_name]);
      }
    }

    return $normalized;
  }

}
