<?php

namespace Drupal\lod\Plugin\LodNormalizer\entity;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\lod\Value\NormalizerContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:paragraph",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\paragraphs\ParagraphInterface",
 *   weight = 900,
 * )
 */
class Paragraph extends ContentEntity {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SerializerInterface $serializer,
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer,
    LanguageManagerInterface $language_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->languageManager = $language_manager;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $configuration['serializer'],
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($paragraph, NormalizerContext $context) {
    $view_builder = $this->entityTypeManager->getViewBuilder('paragraph');

    /* @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    if (!$paragraph->isTranslatable() || !$paragraph->getParentEntity()->isTranslatable()) {
      $normalized = $view_builder->view($paragraph);
      $normalized = $this->renderer->render($normalized);
      $normalized = $normalized->jsonSerialize();
    }
    else {
      $normalized = [];
      foreach (array_keys($this->languageManager->getLanguages()) as $langcode) {
        $build = $view_builder->view($paragraph, 'full', $langcode);
        $rendered = $this->renderer->render($build);
        $normalized[$langcode] = $rendered->jsonSerialize();
      }
    }

    return $normalized;
  }

}
