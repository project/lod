<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * Translatable field item normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:uuid_item",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Field\Plugin\Field\FieldType\UuidItem",
 *   weight = 1000,
 * )
 */
class UuidItem extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    /* @var \Drupal\Core\Field\Plugin\Field\FieldType\UuidItem $field */
    if (!isset($context['namespace'])) {
      return $field->getString();
    }
    return [
      'merge_parent' => TRUE,
      '@id' => sprintf('%s/%s', $context['namespace'], $field->getString()),
    ];
  }

}
