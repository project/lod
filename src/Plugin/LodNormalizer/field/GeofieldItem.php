<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Translatable field item normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:geofield_item",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\geofield\Plugin\Field\FieldType\GeofieldItem",
 *   weight = 1000,
 * )
 */
class GeofieldItem extends BasePlugin {

  /**
   * The geoPhpWrapper service.
   *
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geoPhpWrapper;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SerializerInterface $serializer, GeoPHPInterface $geophp_wrapper) {
    $this->geoPhpWrapper = $geophp_wrapper;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $configuration['serializer'],
      $container->get('geofield.geophp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    /* @var \Drupal\geofield\Plugin\Field\FieldType\GeofieldItem $field */
    $geom = $this->geoPhpWrapper->load($field->get('value')->getString());
    $parent_id = $field->getParent()->getParent()->getValue()->id();

    return [
      '@context' => [
        'geosparql' => 'http://www.opengis.net/ont/geosparql#',
        'asWKT' => [
          '@type' => 'geosparql:wktLiteral',
          '@id' => 'geosparql:asWKT',
        ],
      ],
      '@type' => 'geosparql:Geometry',
      '@id' => sprintf('%s/geometry/%s/%s', $context->getNamespace(), $parent_id, $geom->out('geohash')),
      'asWKT' => $geom->out('wkt'),
    ];
  }

}
