<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * Default entity normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:entity_reference",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem",
 *   weight = 1000,
 * )
 */
class ContentEntityReference extends BasePlugin {

  /**
   * Export the UUID of the target entity.
   *
   * {@inheritdoc}
   */
  public function normalize($object, NormalizerContext $context) {
    /* @var $object \Drupal\Core\Field\FieldItemInterface */
    $target_entity = $object->get('entity')->getValue();

    if ($this->isEntityExcluded($target_entity, $context)) {
      return NULL;
    }

    $normalized = $this->serializer->normalize($target_entity->get('uuid'), JsonLdEncoder::FORMAT, (array) $context);
    return $normalized[0]['@id'];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($object) {
    /* @var $object \Drupal\Core\Field\FieldItemInterface */
    $target_entity = $object->get('entity')->getValue();

    // Only support content entities.
    return $target_entity instanceof ContentEntityInterface;
  }

  /**
   * Check if an entity is excluded from being exported.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Normalizer context.
   *
   * @return bool
   *   True if the entity should be excluded.
   */
  protected function isEntityExcluded(ContentEntityInterface $entity, NormalizerContext $context) {
    $excluded = $context->getExcludedEntityTypes();
    $type = $entity->getEntityTypeId();

    // Check if entity type is in the excluded list.
    if (!array_key_exists($type, $excluded)) {
      return FALSE;
    }

    // Return true if there are no bundles selected or the bundle is in the
    // excluded bundles list.
    $excluded_bundles = $excluded[$type];
    return empty($excluded_bundles) || \in_array($entity->bundle(), $excluded_bundles, TRUE);
  }

}
