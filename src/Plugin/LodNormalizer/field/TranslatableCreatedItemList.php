<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\lod\Value\FieldItemListDetails;
use Drupal\lod\Value\NormalizerContext;

/**
 * Translatable created item normalizer plugin.
 *
 * Most LOD harvester and smart agents can't handle a multi-value created field.
 * Reduces the array to the single oldest value.
 *
 * @LodNormalizer(
 *   id = "lod:translatable_created_item_list",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Field\FieldItemListInterface",
 *   weight = 980,
 * )
 */
class TranslatableCreatedItemList extends TranslatableFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    $fieldDetails = new FieldItemListDetails($field);
    $normalized = parent::normalize($field, $context);

    $dates = $fieldDetails->isMultiValue()
      ? array_merge(...array_values($normalized))
      : $normalized;

    sort($dates);

    // Return only the oldest value.
    return reset($dates);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($field) {
    if (!parent::supportsNormalization($field)) {
      return FALSE;
    }

    /* @var \Drupal\Core\Field\FieldItemListInterface $field */
    return $field->getName() === 'created';
  }

}
