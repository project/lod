<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Value\NormalizerContext;

/**
 * Embed entity references by normalizing their target.
 *
 * @LodNormalizer(
 *   id = "lod:entity_reference_embed",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem",
 *   weight = 990,
 * )
 */
class ContentEntityReferenceEmbed extends ContentEntityReference {

  /**
   * {@inheritdoc}
   */
  public function normalize($object, NormalizerContext $context) {
    /* @var $object \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem */
    if ($namespace = $this->isReferenceField($object, $context)) {
      $context->setNamespace($namespace);
      return parent::normalize($object, $context);
    }

    /* @var $object \Drupal\Core\Field\FieldItemInterface */
    $target_entity = $object->get('entity')->getValue();

    if ($this->isEntityExcluded($target_entity, $context)) {
      return NULL;
    }

    return $this->serializer->normalize($target_entity, JsonLdEncoder::FORMAT);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($object) {
    if (!parent::supportsNormalization($object)) {
      return FALSE;
    }

    // Don't support fields where the target and parent type are the same
    // because there is a risk of creating an endless loop (e.g. related
    // content), so just return the id.
    /* @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $parent */
    $parent = $object->getParent();
    $target_settings = $parent->getSettings();
    $parent_entity = $parent->getEntity();
    if ($target_settings['target_type'] !== $parent_entity->getEntityTypeId()) {
      return TRUE;
    }

    // If the types and there are no bundles we can't support it.
    if (!isset($target_settings['handler_settings']['target_bundles'])) {
      return FALSE;
    }

    // If the types are the same but the bundles are not we can support it.
    return !in_array($parent_entity->bundle(), $target_settings['handler_settings']['target_bundles'], TRUE);
  }

  /**
   * Check if this field needs to be exported as a reference field.
   *
   * @param \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $object
   *   The field to check.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   The normalization context.
   *
   * @return string|bool
   *   The field's namespace if it is a reference field, FALSE if not.
   */
  protected function isReferenceField(EntityReferenceItem $object, NormalizerContext $context) {
    $field_definition = $object->getParent()->getFieldDefinition();

    if (!($field_definition instanceof ConfigEntityInterface)) {
      return FALSE;
    }

    $field_name = $field_definition->get('field_name');
    if (isset($context['reference_field'][$field_name])) {
      return $context['reference_field'][$field_name];
    }

    return FALSE;
  }

}
