<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * Address field item normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:address_item",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\address\Plugin\Field\FieldType\AddressItem",
 *   weight = 1000,
 * )
 */
class AddressItem extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    /* @var \Drupal\address\Plugin\Field\FieldType\AddressItem $field */
    return [
      '@type' => 'schema:PostalAddress',
      'addressCountry' => $field->getCountryCode(),
      'addressLocality' => $field->getLocality(),
      'postalCode' => $field->getPostalCode(),
      'streetAddress' => implode(' ', array_filter([$field->getAddressLine1(), $field->getAddressLine2()])),
    ];
  }

}
