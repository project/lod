<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\Component\Utility\NestedArray;
use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * Entity reference field item list normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:entity_reference_field_item_list",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   weight = 1000,
 * )
 */
class EntityReferenceFieldItemList extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function normalize($item_list, NormalizerContext $context) {
    /* @var $item_list \Drupal\Core\Field\EntityReferenceFieldItemList */
    $normalized = [];

    foreach ($item_list as $item) {
      $normalized_field = $this->serializer->normalize($item, JsonLdEncoder::FORMAT, (array) $context);

      if (!empty($normalized_field['merge_parent'])) {
        unset($normalized_field['merge_parent']);
        $normalized = NestedArray::mergeDeep($normalized, $normalized_field);
        continue;
      }

      $normalized[] = $normalized_field;
    }

    return $this->removeEmptyResults($normalized);
  }

  /**
   * Remove empty values from normalized array.
   *
   * @param array $normalized
   *   The normalized array.
   *
   * @return array|null
   *   Array without empty values, null if nothing is left.
   */
  protected function removeEmptyResults(array $normalized) {
    $normalized = array_filter($normalized);
    return empty($normalized) ? NULL : $normalized;
  }

}
