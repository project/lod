<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\FieldItemListDetails;
use Drupal\lod\Value\NormalizerContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Translatable field item normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:translatable_field_item_list",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Field\FieldItemListInterface",
 *   weight = 990,
 * )
 */
class TranslatableFieldItemList extends BasePlugin {

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SerializerInterface $serializer, LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $configuration['serializer'],
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    $normalized = [];

    foreach (array_keys($this->languageManager->getLanguages()) as $langcode) {
      if (!$this->translationExists($field, $langcode)) {
        continue;
      }

      $normalized[$langcode] = $this->translateField($field, $langcode, (array) $context);
    }

    return array_filter($normalized);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($field) {
    /* @var \Drupal\Core\Field\FieldItemListInterface $field */
    $fieldDefinition = $field->getDataDefinition();

    // Entities should handle their own translations a language map can't have
    // an object: https://www.w3.org/TR/json-ld/#dfn-language-map
    if ($field instanceof EntityReferenceFieldItemListInterface) {
      return FALSE;
    }

    // Test if field has a parent entity.
    $parent = $field->getParent();
    if (!($parent instanceof EntityAdapter)) {
      return FALSE;
    }

    // Test if the entity is translatable.
    $entity = $parent->getValue();
    if (!($entity instanceof TranslatableInterface) || !$entity->isTranslatable()) {
      return FALSE;
    }

    // Test if the field is translatable.
    if (!($fieldDefinition instanceof FieldDefinitionInterface) || !$fieldDefinition->isTranslatable()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Translate and normalize field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   Field to normalize and translate..
   * @param string $langcode
   *   Language code to translate the field in.
   * @param array $context
   *   Normalization context.
   *
   * @return array|null
   *   Translated and normalized field or null if not available.
   */
  protected function translateField(FieldItemListInterface $field, $langcode, array $context) {
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $field->getParent()->getValue();
    $fieldDefinition = $field->getDataDefinition();
    $fieldDetails = new FieldItemListDetails($field);

    $translatedField = $entity->getTranslation($langcode)->get($fieldDefinition->getName());

    $normalized = [];
    foreach ($translatedField as $key => $item) {
      $normalized[$key] = $this->serializer->normalize($item, JsonLdEncoder::FORMAT, $context);
    }

    return $fieldDetails->isMultiValue() ? $normalized : array_shift($normalized);
  }

  /**
   * Check if the translation exists.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field to check the translation for.
   * @param string $langCode
   *   The langcode to check.
   *
   * @return bool
   *   Exists.
   */
  private function translationExists(FieldItemListInterface $field, string $langCode): bool {
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $field->getParent()->getValue();
    return $entity->hasTranslation($langCode);
  }

}
