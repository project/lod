<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * Translatable field item normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:text_item",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\text\Plugin\Field\FieldType\TextItemBase",
 *   weight = 1000,
 * )
 */
class TextItem extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    /* @var \Drupal\text\Plugin\Field\FieldType\TextItemBase $field */
    return $field->get('value')->getString();
  }

}
