<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * Field item normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:field_item",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\Core\Field\FieldItemInterface",
 *   weight = 1999,
 * )
 */
class FieldItem extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    /** @var \Drupal\Core\Field\FieldItemInterface $field */
    $value = $field->getProperties();

    if (count($value) === 1) {
      $value = reset($value);
    }

    return $this->serializer->normalize($value, JsonLdEncoder::FORMAT, (array) $context);
  }

}
