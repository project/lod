<?php

namespace Drupal\lod\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;
use Drupal\image\Plugin\Field\FieldType\ImageItem as ImageItemType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Image field item normalizer plugin.
 *
 * @LodNormalizer(
 *   id = "lod:image_item",
 *   format = "json_ld",
 *   supportedClass = "\Drupal\image\Plugin\Field\FieldType\ImageItem",
 *   weight = 950,
 * )
 */
class ImageItem extends BasePlugin {

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SerializerInterface $serializer, LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $configuration['serializer'],
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($field, NormalizerContext $context) {
    /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $field */
    /** @var \Drupal\file\FileInterface $file */
    $file = $field->get('entity')->getValue();
    $url = $file->createFileUrl(FALSE);

    return [
      '@type' => 'schema:ImageObject',
      '@id' => $url,
      'url' => $url,
      'caption' => $this->getCaption($field, $context),
      'name' => $field->get('title'),
    ];
  }

  /**
   * Get the caption.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $image
   *   Image to get the caption for.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Normalization context.
   *
   * @return string|array
   *   An array of translated strings or a single string if the field is not
   *   translatable.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getCaption(ImageItemType $image, NormalizerContext $context) {
    $entity = $image->getEntity();
    if (!($entity instanceof TranslatableInterface) || !$entity->isTranslatable() || !$image->getFieldDefinition()->isTranslatable()) {
      return $image->get('alt')->getString();
    }

    $normalized = [];
    foreach (array_keys($this->languageManager->getLanguages()) as $langcode) {
      $normalized[$langcode] = $this->getTranslatedCaption($image, $langcode, $context);
    }

    return $normalized;
  }

  /**
   * Get the translated caption.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $image
   *   Image to get the translated caption for.
   * @param string $langcode
   *   Language code to translate the caption in.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Normalization context.
   *
   * @return string
   *   Translated caption.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function getTranslatedCaption(ImageItemType $image, $langcode, NormalizerContext $context) {
    $field = $image->getParent();
    /* @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $field->getParent()->getValue();
    $field_definition = $field->getDataDefinition();

    if (!$entity->hasTranslation($langcode)) {
      return '';
    }

    $translated_field = $entity->getTranslation($langcode)->get($field_definition->getName());
    return $translated_field->first()->get('alt')->getString();
  }

}
