<?php

namespace Drupal\lod;

/**
 * Class LodNormalizerManager.
 */
interface LodNormalizerManagerInterface {

  /**
   * Get definitions by format.
   *
   * @param string $format
   *   The format to get the definitions for.
   *
   * @return array
   *   Plugin definitions.
   */
  public function getDefinitionsByFormat($format);

}
