<?php

namespace Drupal\lod\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to configure the LOD endpoint.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lod_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('lod.config');

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint URL'),
      '#description' => $this->t('Provide the endpoint URL including the API version number.'),
      '#default_value' => $config->get('endpoint'),
      '#required' => TRUE,
    ];

    $form['user_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API user key'),
      '#description' => $this->t('Provide the (optional) API user key to access the service.'),
      '#default_value' => $config->get('user_key'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!UrlHelper::isValid($form_state->getValue('endpoint'), TRUE)) {
      $form_state->setErrorByName(
        'endpoint',
        $this->t('Provide an absolute URL.')
      );
      return;
    }

    $form_state->setValue(
      'endpoint',
      rtrim($form_state->getValue('endpoint'), '/') . '/'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('lod.config');
    $config->set('endpoint', $form_state->getValue('endpoint'));
    $config->set('user_key', $form_state->getValue('user_key'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['lod.config'];
  }

}
