<?php

namespace Drupal\lod\Encoder;

use Drupal\serialization\Encoder\JsonEncoder;

/**
 * Encodes data in JSON+LD.
 *
 * Simply respond to JSON+LD format requests using the JSON encoder.
 */
class JsonLdEncoder extends JsonEncoder {

  /**
   * JSON LD format.
   */
  const FORMAT = 'json_ld';

  /**
   * The formats that this Encoder supports.
   *
   * @var string
   */
  protected static $format = [self::FORMAT];

}
