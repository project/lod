<?php

declare(strict_types = 1);

namespace Drupal\lod\Value;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Wrapper around the field to get details needed for the normalizers.
 */
final class FieldItemListDetails {

  /**
   * The field item list the details are about.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  private $fieldItemList;

  /**
   * Create the details object from a given field item list.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $fieldItemList
   *   The field item list.
   */
  public function __construct(FieldItemListInterface $fieldItemList) {
    $this->fieldItemList = $fieldItemList;
  }

  /**
   * Get the cardinality of the field.
   *
   * @return int
   *   The normalized field.
   */
  public function getCardinality(): int {
    $definition = $this->fieldItemList->getFieldDefinition();
    if ($definition instanceof BaseFieldDefinition) {
      return (int) $definition->getCardinality();
    }

    if ($definition instanceof FieldConfig) {
      $storage = $definition->getFieldStorageDefinition();
      return (int) $storage->get('cardinality');
    }

    return 1;
  }

  /**
   * Is this a multi-value field.
   *
   * @return bool
   *   Is multi-value.
   */
  public function isMultiValue(): bool {
    return $this->getCardinality() !== 1;
  }

}
