<?php

namespace Drupal\lod\Value;

use Drupal\Core\Session\AccountInterface;

/**
 * Class NormalizerContext.
 */
class NormalizerContext extends \ArrayIterator {

  /**
   * Add an excluded field.
   *
   * @param string $name
   *   Name of excluded field.
   */
  public function addExcludedField($name) {
    $this->addArrayValue('excluded_field', $name);
  }

  /**
   * Get array of excluded fields.
   *
   * @return array
   *   Excluded fields.
   */
  public function getExcludedFields() {
    return $this->getArrayValue('excluded_field');
  }

  /**
   * Add an included field.
   *
   * @param string $name
   *   Name of included field.
   */
  public function addIncludedField($name) {
    $this->addArrayValue('included_field', $name);
  }

  /**
   * Get array of included fields.
   *
   * @return array
   *   Included fields.
   */
  public function getIncludedFields() {
    return $this->getArrayValue('included_field');
  }

  /**
   * Add excluded entity type.
   *
   * Used in entity reference fields.
   *
   * @param string $type
   *   The entity type id.
   * @param array $bundles
   *   The entity bundles to exclude, if none are give all bundles are excluded.
   */
  public function addExcludedEntityType($type, array $bundles = []) {
    $this['excluded_entity_type'][$type] = $bundles;
  }

  /**
   * Get array of excluded entity types.
   *
   * Key/value array where the key is the entity type id and the value an array
   * of bundles to excluded, if an empty array is given as value all bundles
   * should be excluded.
   *
   * @return array
   *   Array of excluded entities.
   */
  public function getExcludedEntityTypes() {
    return $this->getArrayValue('excluded_entity_type');
  }

  /**
   * Add field translation.
   *
   * @param string $original_name
   *   The original Drupal field name.
   * @param string $new_name
   *   The LOD name of the field.
   */
  public function addFieldTranslation($original_name, $new_name) {
    $this['field_translation'][$original_name] = $new_name;
  }

  /**
   * Get array of field translations.
   *
   * @return array
   *   Key/value array, where key is the original name and value the new name.
   */
  public function getFieldTranslations() {
    return $this->getArrayValue('field_translation');
  }

  /**
   * Add reference field.
   *
   * Reference fields only export their id.
   *
   * @param string $name
   *   The field name.
   * @param string $namespace
   *   The namespace uri to use in the id generation.
   */
  public function addReferenceField($name, $namespace) {
    $this['reference_field'][$name] = $namespace;
  }

  /**
   * Get array of field translations.
   *
   * @return array
   *   Key/value array, where key is the original name and value the new name.
   */
  public function getReferenceFields() {
    return $this->getArrayValue('reference_field');
  }

  /**
   * Set account.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account.
   */
  public function setAccount(AccountInterface $account) {
    $this['account'] = $account;
  }

  /**
   * Get account.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   User account.
   */
  public function getAccount() {
    return $this->getValue('account');
  }

  /**
   * Set namespace uri.
   *
   * @param string $namespace
   *   Namespace uri.
   */
  public function setNamespace($namespace) {
    $this['namespace'] = $namespace;
  }

  /**
   * Get namespace uri.
   *
   * @return string
   *   Namespace uri.
   */
  public function getNamespace() {
    return $this->getValue('namespace');
  }

  /**
   * Get an array value.
   *
   * @param string $name
   *   Name of the value.
   *
   * @return mixed
   *   The value or null if not set.
   */
  protected function getValue($name) {
    return isset($this[$name]) ? $this[$name] : NULL;
  }

  /**
   * Get an array value.
   *
   * @param string $name
   *   Name of the value.
   *
   * @return array
   *   The array value.
   */
  protected function getArrayValue($name) {
    return isset($this[$name]) && \is_array($this[$name]) ? $this[$name] : [];
  }

  /**
   * Get an array value.
   *
   * @param string $name
   *   Name of the value.
   * @param mixed $value
   *   Value to set.
   */
  protected function addArrayValue($name, $value) {
    if (!isset($this[$name]) || !\in_array($value, $this[$name], TRUE)) {
      $this[$name][] = $value;
    }
  }

}
