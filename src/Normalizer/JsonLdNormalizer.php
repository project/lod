<?php

namespace Drupal\lod\Normalizer;

use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\LodNormalizerManagerInterface;
use Drupal\lod\Value\NormalizerContext;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Json LD normalizer.
 */
class JsonLdNormalizer implements LodNormalizerInterface {

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The LOD Normalizer plugin manager.
   *
   * @var \Drupal\lod\LodNormalizerManager
   */
  protected $lodNormalizerManager;

  /**
   * Whether to add the debug info in the output.
   *
   * @var bool
   */
  protected $debug;

  /**
   * Plugins for json_ld.
   *
   * @var array
   */
  protected $plugins;

  /**
   * Plugins by class.
   *
   * @var array
   */
  protected $pluginsByClass;

  /**
   * Initiated plugins.
   *
   * @var \Drupal\lod\LodNormalizerPluginInterface[]
   */
  protected $pluginInstances;

  /**
   * JsonLdNormalizer constructor.
   *
   * @param \Drupal\lod\LodNormalizerManagerInterface $lod_normalizer_manager
   *   The LOD Normalizer plugin manager.
   * @param bool $debug
   *   (optional) Whether to add the debug info in the output.
   */
  public function __construct(LodNormalizerManagerInterface $lod_normalizer_manager, $debug = FALSE) {
    $this->lodNormalizerManager = $lod_normalizer_manager;
    $this->debug = $debug;
    $this->plugins = $this->lodNormalizerManager->getDefinitionsByFormat(JsonLdEncoder::FORMAT);
  }

  /**
   * {@inheritdoc}
   */
  public function setSerializer(SerializerInterface $serializer) {
    $this->serializer = $serializer;
  }

  /**
   * Get a supported normalizer plugin.
   *
   * @param object $object
   *   The object to get the normalizer for.
   *
   * @return \Drupal\lod\LodNormalizerPluginInterface|bool
   *   A supported normalizer plugin if there is one, FALSE otherwise.
   */
  protected function getNormalizerPlugin($object) {
    // Return the first plugin that returns true on supportsNormalization().
    foreach ($this->getPluginsForObjectType($object) as $plugin) {
      $normalizerPlugin = $this->getPlugin($plugin['id']);
      if ($normalizerPlugin->supportsNormalization($object)) {
        return $normalizerPlugin;
      }
    }

    return FALSE;
  }

  /**
   * Get all possible plugins for this object type.
   *
   * Does not initiate the plugin, only checks based on supportedClass
   * annotation.
   *
   * @param object $object
   *   The object to get the plugins for.
   *
   * @return array
   *   Array of possible plugins for this object type.
   */
  protected function getPluginsForObjectType($object) {
    $class = get_class($object);

    if (!isset($this->pluginsByClass[$class])) {
      // Remove plugins incompatible by class.
      $plugins = array_filter($this->plugins, function ($plugin) use ($object) {
        return !isset($plugin['supportedClass']) || $object instanceof $plugin['supportedClass'];
      });
      $this->pluginsByClass[$class] = $plugins;
    }

    return $this->pluginsByClass[$class];
  }

  /**
   * Get a plugin instance.
   *
   * @param string $plugin_id
   *   The id of the plugin to get.
   *
   * @return \Drupal\lod\LodNormalizerPluginInterface
   *   The plugin instance.
   */
  protected function getPlugin($plugin_id) {
    if (!isset($this->pluginInstances[$plugin_id])) {
      $this->pluginInstances[$plugin_id] = $this->lodNormalizerManager->createInstance($plugin_id, ['serializer' => $this->serializer]);
    }

    return $this->pluginInstances[$plugin_id];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    // If the format isn't JSON-LD or we aren't dealing with an object return
    // now.
    if ($format !== JsonLdEncoder::FORMAT || !is_object($data)) {
      return FALSE;
    }

    return (bool) $this->getNormalizerPlugin($data);
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $normalizer = $this->getNormalizerPlugin($object);
    $data = $normalizer->normalize($object, new NormalizerContext($context));

    if ($this->debug) {
      $data = is_array($data) ? $data : ['original_value' => $data];
      $data['@debug'] = [
        'currentPluginClass' => get_class($normalizer),
        'objectClass' => get_class($object),
        'child' => isset($data['@debug']) ? $data['@debug'] : NULL,
      ];
    }

    return $data;
  }

}
