<?php

namespace Drupal\lod\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;

/**
 * Defines the interface of JSON-LD normalizers.
 */
interface LodNormalizerInterface extends NormalizerInterface, SerializerAwareInterface {}
