<?php

namespace Drupal\lod\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an lod normalizer annotation object.
 *
 * @Annotation
 */
class LodNormalizer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The supported format.
   *
   * @var string
   */
  public $format;

  /**
   * The supported class.
   *
   * @var string
   */
  public $supportedClass;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

}
