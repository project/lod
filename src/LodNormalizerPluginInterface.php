<?php

namespace Drupal\lod;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\lod\Value\NormalizerContext;

/**
 * Interface LodNormalizerInterface.
 */
interface LodNormalizerPluginInterface extends ContainerFactoryPluginInterface {

  /**
   * Confirm if an object can be normalized.
   *
   * Useful when the normalizer only supports certain implementations of the
   * supported class. E.g.: only article nodes instead of all nodes.
   *
   * @param object $object
   *   Object to normalize.
   *
   * @return bool
   *   True if normalizer supports this object.
   */
  public function supportsNormalization($object);

  /**
   * Normalizes an object into an array.
   *
   * @param object $object
   *   Object to normalize.
   * @param \Drupal\lod\Value\NormalizerContext $context
   *   Context options for the normalizer.
   *
   * @return array
   *   The normalized object as an array.
   */
  public function normalize($object, NormalizerContext $context);

}
