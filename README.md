# Linked Open Data Export


INTRODUCTION
------------

Drupal module to transform Drupal content to Linked Open Data format (JSON-LD).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/lod

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/lod


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------
 
 * Create a new View

   - Add a display of type "JSON-LD export".
   - Select the entities and filters of the content you want to display as usual
     in the View.
   - Set up a path in the View to access the data.
   - IMPORTANT: In case you are using a pager, select the **full** pager and set
     the "Number of pager links visible" to a high number such as 999999.
     
     
CUSTOMIZATION
-------------
 
The module provides a plugin type "LodNormalizer" which can be implemented in
any other module and provide more customization on the output of the LOD View.

Examples how to implement the plugin can be found in the module's own
implementations in the directory lod/src/Plugin/LodNormalizer.

The main idea is that every class (entity, field, data type, ...) that is being
used while rendering an entity can get a specific implementation using the
LodNormalizer plugin to get rendered as Linked Open Data.


MAINTAINERS
-----------

Current maintainers:
 * Wesley De Vrient (wesleydv) - https://www.drupal.org/user/118660
 * Lennart Van Vaerenbergh (Fernly) - https://www.drupal.org/user/1338426
 * Matthijs Van Assche (Matthijs) - https://www.drupal.org/user/1012260
 * Peter Decuyper (zero2one) - https://www.drupal.org/user/105066
 * Maarten Segers (mpp) - https://www.drupal.org/user/359881

This project has been sponsored by:
 * DIGIPOLIS
   The IT department of the City of Ghent, Belgium.
