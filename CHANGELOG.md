# Changelog

All Notable changes to **Linked Open Data Export**.

## 1.1.1

### Added

* Added permission to access module settings.

## 1.1.0

### Added

* Added PHP 8 support.

## 1.0.2

### Fixed

* Fixed missing `getEntityRepository()` method in the `LodEntityRow` plugin.

## 1.0.1

### Fixed

* Fixed using deprecated File::toUrl() by File::createFileUrl().

### Changed

* Change default branch name to 1.x.

## 1.0.0

### Fixed

* Fixed broken tests.

## 1.0.0-beta1

### Added

* Initial version of the module.
