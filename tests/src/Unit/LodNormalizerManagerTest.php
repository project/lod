<?php

namespace Drupal\Tests\lod\Unit;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\lod\LodNormalizerManager;
use Drupal\Tests\UnitTestCase;

/**
 * LodNormalizerManager units tests.
 *
 * @group lod
 */
class LodNormalizerManagerTest extends UnitTestCase {

  /**
   * Test getDefinitionsByFormat() with no definitions.
   */
  public function testGetDefinitionsByFormatNoDefinitions() {
    $manager = $this->getLodNormalizerManager();

    $this->assertEmpty($manager->getDefinitionsByFormat(''));
  }

  /**
   * Test getDefinitionsByFormat().
   */
  public function testGetDefinitionsByFormatReturnCorrectFormats() {
    $manager = $this->getLodNormalizerManager([
      [
        'id' => 'test',
        'format' => 'unknown format',
      ],
    ]);

    $this->assertEmpty($manager->getDefinitionsByFormat('format'));
  }

  /**
   * Test getDefinitionsByFormat().
   */
  public function testGetDefinitionsByFormatOrderByWeight() {
    $manager = $this->getLodNormalizerManager([
      [
        'id' => 'test1',
        'format' => 'format',
        'weight' => 1000,
      ],
      [
        'id' => 'test2',
        'format' => 'format',
        'weight' => 500,
      ],
    ]);

    $definitions = $manager->getDefinitionsByFormat('format');

    $this->assertSame('test2', $definitions[0]['id']);
    $this->assertSame('test1', $definitions[1]['id']);
  }

  /**
   * Return a LodNormalizerManager class.
   *
   * Return the given plugins via the cache backend.
   *
   * @param array $plugins
   *   The plugins to return.
   *
   * @return \Drupal\lod\LodNormalizerManager
   *   The LodNormalizerManager.
   */
  protected function getLodNormalizerManager(array $plugins = []) {
    $cache_backend = $this->createMock(CacheBackendInterface::class);

    $mocked_cache = new \stdClass();
    $mocked_cache->data = $plugins;
    $cache_backend->expects($this->once())
      ->method('get')
      ->willReturn($mocked_cache);

    $module_handler = $this->createMock(ModuleHandlerInterface::class);
    return new LodNormalizerManager(new \ArrayIterator([]), $cache_backend, $module_handler);
  }

}
