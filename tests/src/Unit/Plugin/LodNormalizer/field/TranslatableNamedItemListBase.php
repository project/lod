<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;

/**
 * Named translatable item list base.
 *
 * @group lod
 */
class TranslatableNamedItemListBase extends UnitTestCase {

  /**
   * Creates a mock field list item.
   *
   * @param string $name
   *   Name for the field.
   * @param bool $has_data_definition
   *   True if field item list must return a data definition.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked field list item.
   */
  protected function createFieldListItemMock($name = 'created', $has_data_definition = TRUE) {
    $mock = $this->prophesize(FieldItemListInterface::class);
    $entity_adapter = $this->prophesize(EntityAdapter::class);
    $entity = $this->prophesize(ContentEntityInterface::class);

    $entity->isTranslatable()
      ->willReturn(TRUE);

    $entity_adapter->getValue()
      ->willReturn($entity);

    $mock->getDataDefinition()
      ->willReturn($has_data_definition ? $this->createDataDefinitionMock() : new \stdClass());

    $mock->getParent()
      ->willReturn($entity_adapter);

    $mock->getName()
      ->willReturn($name);

    return $mock;
  }

  /**
   * Create a complex field list item mock.
   *
   * This field item can be used to test normalize().
   *
   * @param int $cardinality
   *   The cardinality of the field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   Mocked field list item.
   */
  protected function createComplexFieldListItemMock(int $cardinality) {
    $entityMock = $this->prophesize(ContentEntityInterface::class);
    $entityMock
      ->isTranslatable()
      ->willReturn(TRUE);
    $entityMock
      ->hasTranslation(Argument::any())
      ->willReturn(TRUE);
    // Will be called once in each language.
    $entityMock
      ->getTranslation(Argument::any())
      ->willReturn(
        $this->createTranslatedEntityMock($cardinality),
        $this->createTranslatedEntityMock($cardinality)
      );
    $entity = $entityMock->reveal();

    $entityAdapter = $this->prophesize(EntityAdapter::class);
    $entityAdapter
      ->getValue()
      ->willReturn($entity);

    $field = $this->prophesize(FieldItemListInterface::class);
    $field
      ->getParent()
      ->willReturn($entityAdapter->reveal());
    $field
      ->getDataDefinition()
      ->willReturn($this->createDataDefinitionMock());
    $field
      ->getFieldDefinition()
      ->willReturn($this->createBaseFieldDefinitionMock($cardinality));

    return $field->reveal();
  }

  /**
   * Create mocked data definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked data definition.
   */
  protected function createDataDefinitionMock() {
    $data_definition = $this->prophesize(FieldDefinitionInterface::class);

    $data_definition->isTranslatable()
      ->willReturn(TRUE);

    $data_definition->getName()
      ->willReturn('field_name');

    return $data_definition;
  }

  /**
   * Create mocked field definition.
   *
   * @param int $cardinality
   *   The field cardinality.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The mocked field definition.
   */
  private function createBaseFieldDefinitionMock(int $cardinality): BaseFieldDefinition {
    $baseFieldDefinition = $this->prophesize(BaseFieldDefinition::class);

    $baseFieldDefinition
      ->getCardinality()
      ->willReturn($cardinality);

    return $baseFieldDefinition->reveal();
  }

  /**
   * Create a translated entity mock.
   *
   * @param int $cardinality
   *   The cardinality of the field.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Mocked translated entity.
   */
  private function createTranslatedEntityMock(int $cardinality): ContentEntityInterface {
    $translatedEntity = $this->prophesize(ContentEntityInterface::class);

    // Will be called once in each language.
    $translatedEntity
      ->get('field_name')
      ->willReturn($this->createTranslatedFieldMock($cardinality));

    return $translatedEntity->reveal();
  }

  /**
   * Create translated field mock.
   *
   * @param int $cardinality
   *   The cardinality of the field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The mocked translated field.
   */
  private function createTranslatedFieldMock(int $cardinality): FieldItemListInterface {
    $translatedField = $this->prophesize(FieldItemListInterface::class);

    // Create the iterator steps.
    $valid = array_fill(0, $cardinality, TRUE);
    $values = range(0, $cardinality);
    $valid[] = FALSE;

    $translatedField->rewind()->willReturn(NULL);
    $translatedField->next()->willReturn(NULL);
    $translatedField->valid()
      ->willReturn(...$valid);
    $translatedField->key()
      ->willReturn(...array_keys($values));
    $translatedField->current()
      ->willReturn(...$values);

    return $translatedField->reveal();
  }

}
