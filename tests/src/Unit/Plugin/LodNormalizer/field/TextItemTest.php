<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\lod\Plugin\LodNormalizer\field\TextItem;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;
use Symfony\Component\Serializer\Serializer;

/**
 * ContentEntityNormalizer units tests.
 *
 * @group lod
 */
class TextItemTest extends UnitTestCase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $normalizer = new TextItem([], '', [], new Serializer());

    $data = $this->prophesize(TypedDataInterface::class);
    $data->getString()
      ->willReturn('text')
      ->shouldBeCalled();

    $object = $this->prophesize(TextItemBase::class);
    $object->get('value')
      ->willReturn($data)
      ->shouldBeCalled();

    $this->assertEquals('text', $normalizer->normalize($object->reveal(), new NormalizerContext()));
  }

}
