<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\lod\LodNormalizerPluginInterface;
use Drupal\lod\Plugin\LodNormalizer\field\GeofieldItem;
use Drupal\lod\Value\NormalizerContext;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * AddressItem units tests.
 *
 * @group lod
 */
class GeofieldItemTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    new \geoPHP();
  }

  /**
   * Test create().
   */
  public function testCreate() {
    $geo_php = $this->prophesize(GeoPHPInterface::class);
    $container = $this->prophesize(ContainerInterface::class);
    $container->get('geofield.geophp')
      ->willReturn($geo_php)
      ->shouldBeCalled();

    $configuration = [
      'serializer' => new Serializer(),
    ];

    $normalizer = GeofieldItem::create($container->reveal(), $configuration, '', '');
    $this->assertInstanceOf(LodNormalizerPluginInterface::class, $normalizer);
  }

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $geometry = $this->prophesize(\Geometry::class);
    $geometry->out('wkt')
      ->willReturn('wkt string')
      ->shouldBeCalled();
    $geometry->out('geohash')
      ->willReturn('geohashString')
      ->shouldBeCalled();

    $geo_php_wrapper = $this->prophesize(GeoPHPInterface::class);
    $geo_php_wrapper->load('value string')
      ->willReturn($geometry)
      ->shouldBeCalled();

    $normalizer = new GeofieldItem([], '', [], new Serializer(), $geo_php_wrapper->reveal());

    $geofield_item = $this->getGeofieldItemMock();

    $expected = [
      '@context' => [
        'geosparql' => 'http://www.opengis.net/ont/geosparql#',
        'asWKT' => [
          '@type' => 'geosparql:wktLiteral',
          '@id' => 'geosparql:asWKT',
        ],
      ],
      '@type' => 'geosparql:Geometry',
      '@id' => '/geometry/666/geohashString',
      'asWKT' => 'wkt string',
    ];
    $this->assertEquals($expected, $normalizer->normalize($geofield_item->reveal(), new NormalizerContext()));
  }

  /**
   * Get AddressItem field type mock.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy|\Drupal\geofield\Plugin\Field\FieldType\GeofieldItem
   *   Address item mock.
   */
  protected function getGeofieldItemMock() {
    $geofield_item = $this->prophesize('\Drupal\geofield\Plugin\Field\FieldType\GeofieldItem');

    $value = $this->prophesize(TypedDataInterface::class);
    $value->getString()
      ->willReturn('value string');

    $geofield_item->get('value')
      ->willReturn($value)
      ->shouldBeCalled();

    $entity = $this->prophesize(EntityInterface::class);
    $entity->id()
      ->willReturn(666);

    $entity_adapter = $this->prophesize(TypedDataInterface::class);
    $entity_adapter->getValue()
      ->willReturn($entity);

    $field_item_list = $this->prophesize(FieldItemListInterface::class);
    $field_item_list->getParent()
      ->willReturn($entity_adapter);

    $geofield_item->getParent()
      ->willReturn($field_item_list);

    return $geofield_item;
  }

}
