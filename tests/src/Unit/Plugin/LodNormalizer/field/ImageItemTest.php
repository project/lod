<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\TypedData\Plugin\DataType\StringData;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\lod\LodNormalizerPluginInterface;
use Drupal\lod\Plugin\LodNormalizer\field\ImageItem;
use Drupal\lod\Value\NormalizerContext;
use Drupal\field\Entity\FieldConfig;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * ContentEntityNormalizer units tests.
 *
 * @group lod
 */
class ImageItemTest extends UnitTestCase {

  /**
   * Test create().
   */
  public function testCreate() {
    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $container = $this->prophesize(ContainerInterface::class);
    $container->get('language_manager')
      ->willReturn($language_manager)
      ->shouldBeCalled();

    $configuration = [
      'serializer' => new Serializer(),
    ];

    $normalizer = ImageItem::create($container->reveal(), $configuration, '', '');
    $this->assertInstanceOf(LodNormalizerPluginInterface::class, $normalizer);
  }

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $normalizer = new ImageItem([], '', [], new Serializer(), $language_manager->reveal());

    $field = $this->getImageFieldMock();

    $field_definition = $this->prophesize(FieldDefinitionInterface::class);
    $field_definition->isTranslatable()
      ->willReturn(FALSE)
      ->shouldBeCalled();

    $field->getFieldDefinition()
      ->willReturn($field_definition)
      ->shouldBeCalled();

    $expected = [
      '@type' => 'schema:ImageObject',
      '@id' => 'url',
      'url' => 'url',
      'caption' => 'alt1',
      'name' => 'title',
    ];

    $this->assertEquals($expected, $normalizer->normalize($field->reveal(), new NormalizerContext()));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithTranslation() {
    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $language_manager->getLanguages()
      ->willReturn([
        'nl' => 'nl',
        'fr' => 'fr',
        'en' => 'en'
    ]);

    $normalizer = new ImageItem([], '', [], new Serializer(), $language_manager->reveal());

    $field = $this->getTranslatedImageFieldMock();

    $expected = [
      '@type' => 'schema:ImageObject',
      '@id' => 'url',
      'url' => 'url',
      'caption' => [
        'nl' => 'alt1',
        'fr' => 'alt2',
        'en' => '',
      ],
      'name' => 'title',
    ];

    $this->assertEquals($expected, $normalizer->normalize($field->reveal(), new NormalizerContext()));
  }

  /**
   * Image field mock.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy|\Drupal\image\Plugin\Field\FieldType\ImageItem
   *   Image field mock.
   */
  protected function getImageFieldMock() {
    $file = $this->prophesize(FileInterface::class);
    $file->createFileUrl(FALSE)
      ->willReturn('url')
      ->shouldBeCalled();

    $data = $this->prophesize(TypedDataInterface::class);
    $data->getValue()
      ->willReturn($file)
      ->shouldBeCalled();

    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->isTranslatable()
      ->willReturn(TRUE);

    $field = $this->prophesize('\Drupal\image\Plugin\Field\FieldType\ImageItem');
    $field->getEntity()
      ->willReturn($entity)
      ->shouldBeCalled();
    $field->get('entity')
      ->willReturn($data)
      ->shouldBeCalled();
    $field->get('title')
      ->willReturnArgument(0)
      ->shouldBeCalled();

    $string_data = $this->prophesize(StringData::class);
    $string_data->getString()
      ->willReturn('alt1', 'alt2');

    $field->get('alt')
      ->willReturn($string_data);

    return $field;
  }

  /**
   * Translated image field mock.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy|\Drupal\image\Plugin\Field\FieldType\ImageItem
   *   Translated image field mock.
   */
  protected function getTranslatedImageFieldMock() {
    $field = $this->getImageFieldMock();

    $field_definition = $this->prophesize(FieldDefinitionInterface::class);
    $field_definition->isTranslatable()
      ->willReturn(TRUE);

    $field->getFieldDefinition()
      ->willReturn($field_definition);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity_adapter = $this->prophesize(EntityAdapter::class);
    $field_config = $this->prophesize(FieldConfig::class);
    $file_field = $this->prophesize(FileFieldItemList::class);

    $entity->isTranslatable()
      ->willReturn(TRUE);
    $entity->hasTranslation(Argument::type('string'))
      ->willReturn(TRUE, TRUE, FALSE);
    $entity->getTranslation(Argument::type('string'))
      ->willReturn($entity);
    $entity->get('field_name')
      ->willReturn($file_field);

    $entity_adapter->getValue()
      ->willReturn($entity);

    $field_config->getName()
      ->willReturn('field_name');

    $file_field->getParent()
      ->willReturn($entity_adapter);
    $file_field->getDataDefinition()
      ->willReturn($field_config);
    $file_field->first()
      ->willReturn($field);

    $field->getParent()
      ->willReturn($file_field);

    return $field;
  }

}
