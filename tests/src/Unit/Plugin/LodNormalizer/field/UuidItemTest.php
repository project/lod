<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\lod\Plugin\LodNormalizer\field\UuidItem;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Serializer\Serializer;

/**
 * ContentEntityNormalizer units tests.
 *
 * @group lod
 */
class UuidItemTest extends UnitTestCase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $normalizer = new UuidItem([], '', [], new Serializer());

    $object = $this->prophesize('Drupal\Core\Field\Plugin\Field\FieldType\UuidItem');
    $object->getString()
      ->willReturn('UUID');

    $this->assertEquals('UUID', $normalizer->normalize($object->reveal(), new NormalizerContext()));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithNamespace() {
    $normalizer = new UuidItem([], '', [], new Serializer());

    $object = $this->prophesize('Drupal\Core\Field\Plugin\Field\FieldType\UuidItem');
    $object->getString()
      ->willReturn('UUID');

    $context = new NormalizerContext();
    $context->setNamespace('http://example.com');

    $expected = [
      'merge_parent' => TRUE,
      '@id' => 'http://example.com/UUID',
    ];
    $this->assertEquals($expected, $normalizer->normalize($object->reveal(), $context));
  }

}
