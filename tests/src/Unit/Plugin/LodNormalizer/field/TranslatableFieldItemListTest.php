<?php

declare(strict_types = 1);

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\lod\LodNormalizerPluginInterface;
use Drupal\lod\Plugin\LodNormalizer\field\TranslatableFieldItemList;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * @covers \Drupal\lod\Plugin\LodNormalizer\field\TranslatableFieldItemList
 *
 * @group lod
 */
class TranslatableFieldItemListTest extends UnitTestCase {

  /**
   * Test create().
   */
  public function testCreate() {
    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $container = $this->prophesize(ContainerInterface::class);
    $container->get('language_manager')
      ->willReturn($language_manager)
      ->shouldBeCalled();

    $configuration = [
      'serializer' => new Serializer(),
    ];

    $normalizer = TranslatableFieldItemList::create($container->reveal(), $configuration, '', '');
    $this->assertInstanceOf(LodNormalizerPluginInterface::class, $normalizer);
  }

  /**
   * Test supportsNormalization().
   */
  public function testsupportsNormalization() {
    $field = $this->createFieldListItemMock();
    $normalizer = $this->getTranslatableFieldItemListNormalizer();
    $this->assertTrue($normalizer->supportsNormalization($field));
  }

  /**
   * Test supportsNormalization().
   *
   * Field definition is not a FieldDefinitionInterface.
   */
  public function testsupportsNormalizationNoDataDefinition() {
    $field = $this->createFieldListItemMock(FALSE);
    $normalizer = $this->getTranslatableFieldItemListNormalizer();
    $this->assertFalse($normalizer->supportsNormalization($field));
  }

  /**
   * Test supportsNormalization().
   *
   * Field definition returns not translatable.
   */
  public function testsupportsNormalizationNotTranslatable() {
    $field = $this->createFieldListItemMock(TRUE, TRUE, TRUE, FALSE);
    $normalizer = $this->getTranslatableFieldItemListNormalizer();
    $this->assertFalse($normalizer->supportsNormalization($field));

    $field = $this->createFieldListItemMock(TRUE, FALSE);
    $this->assertFalse($normalizer->supportsNormalization($field));
  }

  /**
   * Test supportsNormalization().
   *
   * Field has no parent entity.
   */
  public function testsupportsNormalizationNoParentEntity() {
    $field = $this->createFieldListItemMock(TRUE, TRUE, FALSE);
    $normalizer = $this->getTranslatableFieldItemListNormalizer();
    $this->assertFalse($normalizer->supportsNormalization($field));
  }

  /**
   * Test supportsNormalization().
   *
   * Field has is an entity reference.
   */
  public function testSupportsNormalizationIsEntityReference() {
    $field = $this->createFieldListItemMock(TRUE, TRUE, TRUE, TRUE, TRUE);
    $normalizer = $this->getTranslatableFieldItemListNormalizer();
    $this->assertFalse($normalizer->supportsNormalization($field));
  }

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $normalizer = $this->getTranslatableFieldItemListNormalizer();
    $field = $this->createComplexFieldListItemMock(1);
    $normalized = $normalizer->normalize($field, new NormalizerContext());

    $this->assertArrayHasKey('en', $normalized);
    $this->assertArrayNotHasKey('nl', $normalized, 'Field translations without value should not be in the export');
    $this->assertArrayNotHasKey('es', $normalized, 'translation "es" is not available so it should not be in the export');
  }

  /**
   * Test normalize().
   *
   * With multi value field.
   */
  public function testNormalizeWithMultiValueField() {
    $normalizer = $this->getTranslatableFieldItemListNormalizer();
    $field = $this->createComplexFieldListItemMock(2);
    $normalized = $normalizer->normalize($field, new NormalizerContext());

    $this->assertEquals('normalized field', $normalized['en'][1]);
    $this->assertArrayNotHasKey('nl', $normalized, 'Field translations without value should not be in the export');
    $this->assertArrayNotHasKey('es', $normalized, 'translation "es" is not available so it should not be in the export');
  }

  /**
   * Create an instance of TranslatableFieldItemNormalizer.
   *
   * @return \Drupal\lod\Plugin\LodNormalizer\field\TranslatableFieldItemList
   *   TranslatableFieldItemNormalizer.
   */
  protected function getTranslatableFieldItemListNormalizer() {
    $serializer = $this->prophesize(Serializer::class);
    $serializer
      ->normalize(Argument::any(), 'json_ld', Argument::any())
      ->willReturn('normalized field');

    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $language_manager
      ->getLanguages()
      ->willReturn(['en' => 'en', 'nl' => 'nl', 'es' => 'es']);

    return new TranslatableFieldItemList([], '', [], $serializer->reveal(), $language_manager->reveal());
  }

  /**
   * Creates a mock field list item.
   *
   * @param bool $hasDataDefinition
   *   True if field item list must return a data definition.
   * @param bool $fieldTranslatable
   *   Return value for isTranslatable() on the data definition.
   * @param bool $hasParentEntity
   *   True if getParent() must return an EntityAdapter.
   * @param bool $entityTranslatable
   *   Return value for isTranslatable() on the entity.
   * @param bool $isEntityReference
   *   True if field list item must use EntityReferenceFieldItemListInterface.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   Mocked field list item.
   */
  protected function createFieldListItemMock(
    $hasDataDefinition = TRUE,
    $fieldTranslatable = TRUE,
    $hasParentEntity = TRUE,
    $entityTranslatable = TRUE,
    $isEntityReference = FALSE
  ): FieldItemListInterface {
    $class = $isEntityReference ? EntityReferenceFieldItemListInterface::class : FieldItemListInterface::class;

    /** @var \Drupal\Core\Field\FieldItemListInterface $field */
    $field = $this->prophesize($class);

    $dataDefinition = $hasDataDefinition
      ? $this->createDataDefinitionMock($fieldTranslatable)
      : new \stdClass();
    $field
      ->getDataDefinition()
      ->willReturn($dataDefinition);

    if (!$hasParentEntity) {
      $field->getParent()
        ->willReturn(new \stdClass());

      return $field->reveal();
    }

    $entity_adapter = $this->prophesize(EntityAdapter::class);
    $entity = $this->prophesize(ContentEntityInterface::class);

    $entity->isTranslatable()
      ->willReturn($entityTranslatable);

    $entity_adapter->getValue()
      ->willReturn($entity);

    $field->getParent()
      ->willReturn($entity_adapter);

    return $field->reveal();
  }

  /**
   * Create a complex field list item mock.
   *
   * This field item can be used to test normalize().
   *
   * @param int $cardinality
   *   Number of values the list item should have.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   Mocked field list item.
   */
  protected function createComplexFieldListItemMock($cardinality = 1) {
    $entity = $this->prophesize(ContentEntityInterface::class);
    $dataDefinition = $this->createDataDefinitionMock(TRUE);
    $fieldDefinition = $this->createBaseFieldDefinitionMock($cardinality);

    $entityAdapter = $this->prophesize(EntityAdapter::class);
    $entityAdapter
      ->getValue()
      ->willReturn($entity)
      ->shouldBeCalledTimes(5);

    $field = $this->prophesize(FieldItemListInterface::class);
    $field
      ->getParent()
      ->willReturn($entityAdapter)
      ->shouldBeCalledTimes(5);
    $field
      ->getDataDefinition()
      ->willReturn($dataDefinition)
      ->shouldBeCalledTimes(2);
    $field
      ->getFieldDefinition()
      ->willReturn($fieldDefinition);

    $entity->hasTranslation('en')
      ->willReturn(TRUE)
      ->shouldBeCalledTimes(1);
    $entity->hasTranslation('nl')
      ->willReturn(TRUE)
      ->shouldBeCalledTimes(1);
    $entity->hasTranslation('es')
      ->willReturn(FALSE)
      ->shouldBeCalledTimes(1);

    $entity->getTranslation('en')
      ->willReturn($this->createTranslatedEntityMock($cardinality))
      ->shouldBeCalledTimes(1);
    $entity->getTranslation('nl')
      ->willReturn($this->createTranslatedEntityMock(0))
      ->shouldBeCalledTimes(1);

    return $field->reveal();
  }

  /**
   * Create translated entity.
   *
   * @param int $cardinality
   *   Number of values the entity list item should have.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The content entity translation.
   */
  protected function createTranslatedEntityMock(int $cardinality): ContentEntityInterface {
    // Create the iterator steps.
    $valid = array_fill(0, $cardinality, TRUE);
    $values = range(0, $cardinality);
    $valid[] = FALSE;

    $translatedField = $this->prophesize(FieldItemListInterface::class);
    $translatedField->rewind()->willReturn(NULL);
    $translatedField->next()->willReturn(NULL);
    $translatedField->valid()
      ->willReturn(...$valid);
    $translatedField->key()
      ->willReturn(...array_keys($values));
    $translatedField->current()
      ->willReturn(...$values);

    $translatedEntity = $this->prophesize(ContentEntityInterface::class);
    $translatedEntity->get('field_name')
      ->willReturn($translatedField->reveal())
      ->shouldBeCalledTimes(1);

    return $translatedEntity->reveal();
  }

  /**
   * Create mocked data definition.
   *
   * @param bool $translatable
   *   Boolean to indicate if the field is translatable.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   Mocked data definition.
   */
  protected function createDataDefinitionMock($translatable = FALSE) {
    $dataDefinition = $this->prophesize(FieldDefinitionInterface::class);

    $dataDefinition
      ->isTranslatable()
      ->willReturn($translatable);

    if ($translatable) {
      $dataDefinition
        ->getName()
        ->willReturn('field_name');
    }

    return $dataDefinition->reveal();
  }

  /**
   * Create mocked field definition.
   *
   * @param int $cardinality
   *   The field cardinality.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The mocked field definition.
   */
  private function createBaseFieldDefinitionMock(int $cardinality): BaseFieldDefinition {
    $baseFieldDefinition = $this->prophesize(BaseFieldDefinition::class);

    $baseFieldDefinition
      ->getCardinality()
      ->willReturn($cardinality);

    return $baseFieldDefinition->reveal();
  }

}
