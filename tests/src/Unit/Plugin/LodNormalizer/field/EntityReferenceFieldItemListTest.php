<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\lod\Plugin\LodNormalizer\field\EntityReferenceFieldItemList;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Serializer\Serializer;

/**
 * EntityReferenceFieldItemList units tests.
 *
 * @group lod
 */
class EntityReferenceFieldItemListTest extends UnitTestCase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $list = new \ArrayIterator(['item']);

    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize('item', 'json_ld', ['context'])
      ->willReturnArgument(0)
      ->shouldBeCalled();

    $normalizer = new EntityReferenceFieldItemList([], '', [], $serializer->reveal());
    $this->assertEquals(['item'], $normalizer->normalize($list, new NormalizerContext(['context'])));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithParentMerge() {
    $list = new \ArrayIterator(['item']);

    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize('item', 'json_ld', ['context'])
      ->willReturn([
        'merge_parent' => TRUE,
        'test' => 'test',
      ])
      ->shouldBeCalled();

    $normalizer = new EntityReferenceFieldItemList([], '', [], $serializer->reveal());
    $this->assertEquals(['test' => 'test'], $normalizer->normalize($list, new NormalizerContext(['context'])));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithEmtpyReturn() {
    $list = new \ArrayIterator(['item']);

    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize('item', 'json_ld', ['context'])
      ->willReturn(NULL)
      ->shouldBeCalled();

    $normalizer = new EntityReferenceFieldItemList([], '', [], $serializer->reveal());
    $this->assertNull($normalizer->normalize($list, new NormalizerContext(['context'])));
  }

}
