<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\lod\Plugin\LodNormalizer\field\AddressItem;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Serializer\Serializer;

/**
 * AddressItem units tests.
 *
 * @group lod
 */
class AddressItemTest extends UnitTestCase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $normalizer = new AddressItem([], '', [], new Serializer());

    $address_item = $this->getAddressItemMock();
    $address_item->getAddressLine2()
      ->willReturn('address_line_2')
      ->shouldBeCalled();

    $expected = [
      '@type' => 'schema:PostalAddress',
      'addressCountry' => 'country_code',
      'addressLocality' => 'locality',
      'postalCode' => 'postal_code',
      'streetAddress' => 'address_line_1 address_line_2',
    ];
    $this->assertEquals($expected, $normalizer->normalize($address_item->reveal(), new NormalizerContext()));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithNoAddressLine2() {
    $normalizer = new AddressItem([], '', [], new Serializer());

    $address_item = $this->getAddressItemMock();
    $address_item->getAddressLine2()
      ->willReturn('')
      ->shouldBeCalled();

    $expected = [
      '@type' => 'schema:PostalAddress',
      'addressCountry' => 'country_code',
      'addressLocality' => 'locality',
      'postalCode' => 'postal_code',
      'streetAddress' => 'address_line_1',
    ];
    $this->assertEquals($expected, $normalizer->normalize($address_item->reveal(), new NormalizerContext()));
  }

  /**
   * Get AddressItem field type mock.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy
   *   Address item mock.
   */
  protected function getAddressItemMock() {
    $address_item = $this->prophesize('Drupal\address\Plugin\Field\FieldType\AddressItem');
    $address_item->getCountryCode()
      ->willReturn('country_code')
      ->shouldBeCalled();
    $address_item->getLocality()
      ->willReturn('locality')
      ->shouldBeCalled();
    $address_item->getPostalCode()
      ->willReturn('postal_code')
      ->shouldBeCalled();
    $address_item->getAddressLine1()
      ->willReturn('address_line_1')
      ->shouldBeCalled();

    return $address_item;
  }

}
