<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityReference as EntityReferenceDataType;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\lod\Plugin\LodNormalizer\field\ContentEntityReferenceEmbed;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Serializer\Serializer;

/**
 * EntityReferenceNormalizer units tests.
 *
 * @group lod
 */
class EntityReferenceEmbedTest extends UnitTestCase {

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationNoParentSupport() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(EntityInterface::class);
    $object = $this->prophesize(EntityReferenceItem::class);

    $reference = $this->prophesize(EntityReferenceDataType::class);
    $reference->getValue()
      ->willReturn($entity)
      ->shouldBeCalled();

    $object->get('entity')
      ->willReturn($reference)
      ->shouldBeCalled();

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());
    $this->assertFalse($normalizer->supportsNormalization($object->reveal()));
  }

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationWithDifferentType() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $object = $this->getEntityReferenceItemForSupportNormalizationMock($entity, ['target_type' => 'not_this_type']);

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());
    $this->assertTrue($normalizer->supportsNormalization($object->reveal()));
  }

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationWithSameTypeNoBundle() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $object = $this->getEntityReferenceItemForSupportNormalizationMock($entity, ['target_type' => 'type']);

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());
    $this->assertFalse($normalizer->supportsNormalization($object->reveal()));
  }

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationWithSameTypeDifferentBundle() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $object = $this->getEntityReferenceItemForSupportNormalizationMock($entity, [
      'target_type' => 'type',
      'handler_settings' => ['target_bundles' => ['not_this_bundle']],
    ]);

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());
    $this->assertTrue($normalizer->supportsNormalization($object->reveal()));
  }

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationWithSameTypeSameBundle() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $object = $this->getEntityReferenceItemForSupportNormalizationMock($entity, [
      'target_type' => 'type',
      'handler_settings' => ['target_bundles' => ['bundle']],
    ]);

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());
    $this->assertFalse($normalizer->supportsNormalization($object->reveal()));
  }

  /**
   * Create an entity reference item that will return the given entity.
   *
   * @param \Prophecy\Prophecy\ObjectProphecy $entity
   *   The mocked entity to return on ->get('entity')->getValue().
   * @param array $settings
   *   Target settings to return on $parent->getSettings().
   *
   * @return \Prophecy\Prophecy\ObjectProphecy
   *   The mocked entity reference item.
   */
  protected function getEntityReferenceItemForSupportNormalizationMock(ObjectProphecy $entity, array $settings) {
    $object = $this->prophesize(EntityReferenceItem::class);

    $reference = $this->prophesize(EntityReferenceDataType::class);
    $reference->getValue()
      ->willReturn($entity)
      ->shouldBeCalled();

    $object->get('entity')
      ->willReturn($reference)
      ->shouldBeCalled();

    $parent_entity = $this->prophesize(EntityInterface::class);

    $parent_reference = $this->prophesize(EntityReferenceFieldItemListInterface::class);
    $parent_reference->getEntity()
      ->willReturn($parent_entity)
      ->shouldBeCalled();
    $parent_reference->getSettings()
      ->willReturn($settings);

    $parent_entity->getEntityTypeId()
      ->willReturn('type')
      ->shouldBeCalled();

    $parent_entity->bundle()
      ->willReturn('bundle');

    $object->getParent()
      ->willReturn($parent_reference)
      ->shouldBeCalled();

    return $object;
  }

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $field_definition = $this->prophesize(ConfigEntityInterface::class);
    $object = $this->getEntityReferenceItemForNormalizeMock($entity, $field_definition);

    $serializer->normalize($entity, 'json_ld')
      ->shouldBeCalled();

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());
    $normalizer->normalize($object->reveal(), new NormalizerContext(['context']));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeNoConfigEntity() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $field_definition = $this->prophesize();
    $object = $this->getEntityReferenceItemForNormalizeMock($entity, $field_definition);

    $serializer->normalize($entity, 'json_ld')
      ->shouldBeCalled();

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());
    $normalizer->normalize($object->reveal(), new NormalizerContext(['context']));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithReferenceField() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->get('uuid')
      ->willReturnArgument(0)
      ->shouldBeCalled();
    $entity->getEntityTypeId()
      ->willReturn('type');

    $field_definition = $this->prophesize(ConfigEntityInterface::class);
    $field_definition->get('field_name')
      ->willReturnArgument(0)
      ->shouldBeCalled();
    $object = $this->getEntityReferenceItemForNormalizeMock($entity, $field_definition);

    $serializer->normalize('uuid', 'json_ld', [
      'reference_field' => [
        'field_name' => 'namespace',
      ],
      'namespace' => 'namespace',
    ])
      ->shouldBeCalled();

    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());

    $context = new NormalizerContext();
    $context->addReferenceField('field_name', 'namespace');

    $normalizer->normalize($object->reveal(), $context);
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithExcludedEntityType() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->getEntityTypeId()
      ->willReturn('type')
      ->shouldBeCalled();

    $field_definition = $this->prophesize(ConfigEntityInterface::class);
    $object = $this->getEntityReferenceItemForNormalizeMock($entity, $field_definition);
    $normalizer = new ContentEntityReferenceEmbed([], '', [], $serializer->reveal());

    $context = new NormalizerContext();
    $context->addExcludedEntityType('type');

    $this->assertNull($normalizer->normalize($object->reveal(), $context));
  }

  /**
   * Create an entity reference item that will return the given entity.
   *
   * @param \Prophecy\Prophecy\ObjectProphecy $entity
   *   The mocked entity to return on ->get('entity')->getValue().
   * @param \Prophecy\Prophecy\ObjectProphecy $field_defintion
   *   Field definition to return on ->getParent()->getFieldDefinition().
   *
   * @return \Prophecy\Prophecy\ObjectProphecy
   *   The mocked entity reference item.
   */
  protected function getEntityReferenceItemForNormalizeMock(ObjectProphecy $entity, ObjectProphecy $field_defintion) {
    $object = $this->prophesize(EntityReferenceItem::class);

    $reference = $this->prophesize(EntityReferenceDataType::class);
    $reference->getValue()
      ->willReturn($entity)
      ->shouldBeCalled();

    $object->get('entity')
      ->willReturn($reference)
      ->shouldBeCalled();

    $parent_reference = $this->prophesize(EntityReferenceFieldItemListInterface::class);
    $parent_reference->getFieldDefinition()
      ->willReturn($field_defintion)
      ->shouldBeCalled();

    $object->getParent()
      ->willReturn($parent_reference)
      ->shouldBeCalled();

    return $object;
  }

}
