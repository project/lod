<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\lod\Plugin\LodNormalizer\field\TranslatableModifiedItemList;
use Drupal\lod\Value\NormalizerContext;
use Prophecy\Argument;
use Symfony\Component\Serializer\Serializer;

/**
 * TranslatableFieldItemList units tests.
 *
 * @group lod
 */
class TranslatableModifiedItemListTest extends TranslatableNamedItemListBase {

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalization() {
    $field = $this->createFieldListItemMock('changed');
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(1);
    $this->assertTrue($normalizer->supportsNormalization($field->reveal()));
  }

  /**
   * Test supportsNormalization().
   *
   * Field name is not changed.
   */
  public function testSupportsNormalizationNotChanged() {
    $field = $this->createFieldListItemMock('other_field');
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(1);
    $this->assertFalse($normalizer->supportsNormalization($field->reveal()));
  }

  /**
   * Test supportsNormalization().
   *
   * Parent does not support normalization.
   */
  public function testSupportsNormalizationNotSupported() {
    $field = $this->createFieldListItemMock('changed', FALSE);
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(1);
    $this->assertFalse($normalizer->supportsNormalization($field));
  }

  /**
   * Newest changed date is retrieved from all translations.
   *
   * @test
   */
  public function newestDateIsRetrievedFromAllTranslations() {
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(FALSE);
    $field = $this->createComplexFieldListItemMock(1);
    $normalized = $normalizer->normalize($field, new NormalizerContext());

    $this->assertEquals('ccc', $normalized);
  }

  /**
   * Newest changed date is retrieved from all language values (multivalue).
   *
   * @test
   */
  public function newestDateDateIsRetrievedFromAllTranslationValues(): void {
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(TRUE);
    $field = $this->createComplexFieldListItemMock(2);
    $normalized = $normalizer->normalize($field, new NormalizerContext());

    $this->assertEquals('dd', $normalized);
  }

  /**
   * Create an instance of TranslatableFieldItemNormalizer.
   *
   * @return \Drupal\lod\Plugin\LodNormalizer\field\TranslatableFieldItemList
   *   TranslatableFieldItemNormalizer.
   */
  protected function getTranslatableCreatedItemListNormalizer(bool $isMultiValue) {
    $normalizerValues = $isMultiValue
      ? ['cc', 'dd', 'aa', 'bb']
      : ['ccc', 'aaa'];

    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize(Argument::any(), 'json_ld', Argument::any())
      ->willReturn(...$normalizerValues);

    $languageManager = $this->prophesize(LanguageManagerInterface::class);
    $languageManager->getLanguages()
      ->willReturn(['en' => 'en', 'es' => 'es']);

    return new TranslatableModifiedItemList([], '', [], $serializer->reveal(), $languageManager->reveal());
  }

}
