<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\lod\Plugin\LodNormalizer\field\FieldItem;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\Serializer\Serializer;

/**
 * FieldItem units tests.
 *
 * @group lod
 */
class FieldItemTest extends UnitTestCase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $data = $this->prophesize(TypedDataInterface::class);

    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize($data, 'json_ld', Argument::any())
      ->willReturn(25);

    $object = $this->prophesize(IntegerItem::class);
    $object->getProperties()
      ->willReturn([$data])
      ->shouldBeCalled();

    $normalizer = new FieldItem([], '', [], $serializer->reveal());

    $this->assertEquals(25, $normalizer->normalize($object->reveal(), new NormalizerContext()));
  }

}
