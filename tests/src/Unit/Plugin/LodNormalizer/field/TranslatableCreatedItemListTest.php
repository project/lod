<?php

declare(strict_types = 1);

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\lod\Plugin\LodNormalizer\field\TranslatableCreatedItemList;
use Drupal\lod\Plugin\LodNormalizer\field\TranslatableFieldItemList;
use Drupal\lod\Value\NormalizerContext;
use Prophecy\Argument;
use Symfony\Component\Serializer\Serializer;

/**
 * @covers \Drupal\lod\Plugin\LodNormalizer\field\TranslatableCreatedItemList
 *
 * @group lod
 */
class TranslatableCreatedItemListTest extends TranslatableNamedItemListBase {

  /**
   * Supports normalization if field is "created" and parent supports it too.
   *
   * @test
   */
  public function normalizationIsSupportedIfFieldIsCreatedAndparentSupportsItToo(): void {
    $field = $this->createFieldListItemMock();
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(FALSE);
    $this->assertTrue($normalizer->supportsNormalization($field->reveal()));
  }

  /**
   * Not supported if the field name is not "created".
   *
   * @test
   */
  public function normalizationIsNotSupportedIfFieldNameIsNotCreated(): void {
    $field = $this->createFieldListItemMock('other_field');
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(FALSE);
    $this->assertFalse($normalizer->supportsNormalization($field->reveal()));
  }

  /**
   * Not supported if parent does not support normalization.
   *
   * @test
   */
  public function normalizationNotSupportedIfParentDoesNotSupportItToo(): void {
    $field = $this->createFieldListItemMock('created', FALSE);
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(FALSE);
    $this->assertFalse($normalizer->supportsNormalization($field));
  }

  /**
   * Oldest changed date is retrieved from all translations.
   *
   * @test
   */
  public function oldestDateIsRetrievedFromAllTranslations(): void {
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(FALSE);
    $field = $this->createComplexFieldListItemMock(1);
    $normalized = $normalizer->normalize($field, new NormalizerContext());

    $this->assertEquals('aaa', $normalized);
  }

  /**
   * Oldest changed date is retrieved from all language values (multivalue).
   *
   * @test
   */
  public function oldestDateDateIsRetrievedFromAllTranslationValues(): void {
    $normalizer = $this->getTranslatableCreatedItemListNormalizer(TRUE);
    $field = $this->createComplexFieldListItemMock(2);
    $normalized = $normalizer->normalize($field, new NormalizerContext());

    $this->assertEquals('aa', $normalized);
  }

  /**
   * Create an instance of TranslatableFieldItemNormalizer.
   *
   * @param bool $isMultiValue
   *   Is the field multi value.
   *
   * @return \Drupal\lod\Plugin\LodNormalizer\field\TranslatableFieldItemList
   *   TranslatableFieldItemNormalizer.
   */
  protected function getTranslatableCreatedItemListNormalizer(
    bool $isMultiValue
  ): TranslatableFieldItemList {
    $normalizedValues = $isMultiValue
      ? ['cc', 'dd', 'aa', 'bb']
      : ['ccc', 'aaa'];

    $serializer = $this->prophesize(Serializer::class);
    $serializer
      ->normalize(Argument::any(), Argument::any(), Argument::any())
      ->willReturn(...$normalizedValues);

    $languageManager = $this->prophesize(LanguageManagerInterface::class);
    $languageManager
      ->getLanguages()
      ->willReturn(['en' => 'en', 'es' => 'es']);

    return new TranslatableCreatedItemList([], '', [], $serializer->reveal(), $languageManager->reveal());
  }

}
