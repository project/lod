<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityReference as EntityReferenceDataType;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\lod\Plugin\LodNormalizer\field\ContentEntityReference;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Serializer\Serializer;

/**
 * EntityReferenceNormalizer units tests.
 *
 * @group lod
 */
class EntityReferenceTest extends UnitTestCase {

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationNoContentEntity() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(EntityInterface::class);
    $object = $this->getEntityReferenceItemMock($entity);

    $normalizer = new ContentEntityReference([], '', [], $serializer->reveal());
    $this->assertFalse($normalizer->supportsNormalization($object->reveal()));
  }

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationWithContentEntity() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $object = $this->getEntityReferenceItemMock($entity);

    $normalizer = new ContentEntityReference([], '', [], $serializer->reveal());
    $this->assertTrue($normalizer->supportsNormalization($object->reveal()));
  }

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize('uuid', 'json_ld', ['context'])
      ->shouldBeCalled();

    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->get('uuid')
      ->willReturnArgument(0)
      ->shouldBeCalled();
    $entity->getEntityTypeId()
      ->willReturn('type');

    $object = $this->getEntityReferenceItemMock($entity);
    $normalizer = new ContentEntityReference([], '', [], $serializer->reveal());
    $normalizer->normalize($object->reveal(), new NormalizerContext(['context']));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithExcludedEntityType() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->getEntityTypeId()
      ->willReturn('type')
      ->shouldBeCalled();

    $object = $this->getEntityReferenceItemMock($entity);
    $normalizer = new ContentEntityReference([], '', [], $serializer->reveal());

    $context = new NormalizerContext();
    $context->addExcludedEntityType('type');

    $this->assertNull($normalizer->normalize($object->reveal(), $context));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithExcludedBundle() {
    $serializer = $this->prophesize(Serializer::class);

    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->getEntityTypeId()
      ->willReturn('type')
      ->shouldBeCalled();
    $entity->bundle()
      ->willReturn('bundle')
      ->shouldBeCalled();

    $object = $this->getEntityReferenceItemMock($entity);
    $normalizer = new ContentEntityReference([], '', [], $serializer->reveal());

    $context = new NormalizerContext();
    $context->addExcludedEntityType('type', ['bundle']);

    $this->assertNull($normalizer->normalize($object->reveal(), $context));
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithNotExcludedBundle() {
    $entity = $this->prophesize(ContentEntityInterface::class);
    $entity->get('uuid')
      ->willReturnArgument(0)
      ->shouldBeCalled();
    $entity->getEntityTypeId()
      ->willReturn('type')
      ->shouldBeCalled();
    $entity->bundle()
      ->willReturn('bundle')
      ->shouldBeCalled();

    $object = $this->getEntityReferenceItemMock($entity);

    $context = new NormalizerContext();
    $context->addExcludedEntityType('type', ['not_this_bundle']);

    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize('uuid', 'json_ld', (array) $context)
      ->shouldBeCalled();

    $normalizer = new ContentEntityReference([], '', [], $serializer->reveal());
    $normalizer->normalize($object->reveal(), $context);
  }

  /**
   * Create an entity reference item that will return the given entity.
   *
   * @param \Prophecy\Prophecy\ObjectProphecy $entity
   *   The mocked entity to return on ->get('entity')->getValue().
   *
   * @return \Prophecy\Prophecy\ObjectProphecy
   *   The mocked entity reference item.
   */
  protected function getEntityReferenceItemMock(ObjectProphecy $entity) {
    $object = $this->prophesize(EntityReferenceItem::class);

    $reference = $this->prophesize(EntityReferenceDataType::class);
    $reference->getValue()
      ->willReturn($entity)
      ->shouldBeCalled();

    $object->get('entity')
      ->willReturn($reference)
      ->shouldBeCalled();

    return $object;
  }

}
