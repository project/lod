<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\Subclasses;

use Drupal\lod\Plugin\LodNormalizer\BasePlugin;
use Drupal\lod\Value\NormalizerContext;

/**
 * A class for testing BaseNormalizer.
 *
 * @codeCoverageIgnore
 */
class Test extends BasePlugin {

  /**
   * {@inheritdoc}
   */
  public function normalize($object, NormalizerContext $context) {
    return 'test';
  }

}
