<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer;

use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Drupal\lod\Plugin\LodNormalizer\DefaultNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * DefaultNormalizer units tests.
 *
 * @group lod
 */
class DefaultNormalizerTest extends UnitTestCase {

  /**
   * Test normalize().
   *
   * @dataProvider normalizeDataProvider
   */
  public function testNormalize($value, $expected) {
    $serializer = $this->createMock(Serializer::class);
    $serializer->method('normalize')
      ->will($this->returnArgument(0));

    $normalizer = new DefaultNormalizer([], '', [], $serializer);
    $normalized = $normalizer->normalize($value, new NormalizerContext());
    $this->assertEquals($expected, $normalized);
  }

  /**
   * Data provider for testNormalize().
   */
  public function normalizeDataProvider() {
    return [
      [new \ArrayIterator(['test', 'test']), ['test', 'test']],
      ['test', 'test'],
    ];
  }

}
