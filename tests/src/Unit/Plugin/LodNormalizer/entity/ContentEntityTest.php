<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\lod\Plugin\LodNormalizer\entity\ContentEntity;
use Drupal\lod\Value\NormalizerContext;
use Prophecy\Argument;
use Symfony\Component\Serializer\Serializer;

/**
 * ContentEntityNormalizer units tests.
 *
 * @group lod
 */
class ContentEntityTest extends EntityTestBase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $normalizer = $this->getContentEntityNormalizer();

    $user = $this->prophesize(AccountInterface::class);
    $content_entity_mock = $this->createMockContentEntity([
      'field_1' => $this->createMockFieldListItem(),
      'field_2' => $this->createMockFieldListItem(),
    ]);

    $context = new NormalizerContext();
    $context->setAccount($user->reveal());

    $normalized = $normalizer->normalize($content_entity_mock->reveal(), $context);

    $this->assertArrayHasKey('field_1', $normalized);
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithMergeParentField() {
    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize(Argument::any(), 'json_ld', Argument::type('array'))
      ->willReturn([
        'merge_parent' => TRUE,
        'test' => 'test',
      ]);
    $normalizer = new ContentEntity([], '', [], $serializer->reveal());

    $content_entity_mock = $this->createMockContentEntity([
      'field_1' => $this->createMockFieldListItem(),
    ]);

    $this->assertEquals(['test' => 'test'], $normalizer->normalize($content_entity_mock->reveal(), new NormalizerContext()));
  }

  /**
   * Test normalize() with excluded fields.
   */
  public function testNormalizeWithExcludedFields() {
    $normalizer = $this->getContentEntityNormalizer();

    $content_entity_mock = $this->createMockContentEntity([
      'vid' => $this->createMockFieldListItem(),
      'field_1' => $this->createMockFieldListItem(),
      'field_2' => $this->createMockFieldListItem(),
    ]);

    $context = new NormalizerContext();
    $context->addExcludedField('field_2');

    $normalized = $normalizer->normalize($content_entity_mock->reveal(), $context);

    $this->assertArrayHasKey('field_1', $normalized);
    // Vid is excluded by default.
    $this->assertArrayNotHasKey('vid', $normalized);
    $this->assertArrayNotHasKey('field_2', $normalized);
  }

  /**
   * Test normalize() with included fields.
   */
  public function testNormalizeWithIncludedFields() {
    $normalizer = $this->getContentEntityNormalizer();

    $content_entity_mock = $this->createMockContentEntity([
      'vid' => $this->createMockFieldListItem(),
      'field_1' => $this->createMockFieldListItem(),
      'field_2' => $this->createMockFieldListItem(),
    ]);

    $context = new NormalizerContext();
    $context->addIncludedField('vid');
    $context->addIncludedField('field_1');

    $normalized = $normalizer->normalize($content_entity_mock->reveal(), $context);

    $this->assertArrayHasKey('field_1', $normalized);
    // Vid is included so the default exclusion is ignored.
    $this->assertArrayHasKey('vid', $normalized);
    $this->assertArrayNotHasKey('field_2', $normalized);
  }

  /**
   * Test normalize() with included fields.
   */
  public function testNormalizeWithFieldRenames() {
    $normalizer = $this->getContentEntityNormalizer();

    $content_entity_mock = $this->createMockContentEntity([
      'title' => $this->createMockFieldListItem(),
      'field_1' => $this->createMockFieldListItem(),
    ]);

    $context = new NormalizerContext();
    $context->addFieldTranslation('title', 'schema:name');

    $normalized = $normalizer->normalize($content_entity_mock->reveal(), $context);

    $this->assertArrayHasKey('schema:name', $normalized);
    $this->assertArrayHasKey('field_1', $normalized);
  }

  /**
   * Test normalize() based on field cardinality.
   */
  public function testNormalizeBasedOnFieldCardinaltiy() {
    $serializer = $this->prophesize(Serializer::class);
    $serializer
      ->normalize(Argument::any(), 'json_ld', Argument::type('array'))
      ->willReturn(
        'normalized field',
        ['normalized field'],
        [['test' => 'normalized field']],
        ['normalized field']
      );

    $normalizer = new ContentEntity([], '', [], $serializer->reveal());

    $content_entity_mock = $this->createMockContentEntity([
      'field_cardinality_1' => $this->createMockFieldListItem(1),
      'field_cardinality_1_with_array' => $this->createMockFieldListItem(1),
      'field_cardinality_1_nested' => $this->createMockFieldListItem(1),
      'field_cardinality_minus1' => $this->createMockFieldListItem(),
    ]);

    $expected = [
      'field_cardinality_1' => 'normalized field',
      'field_cardinality_1_with_array' => 'normalized field',
      'field_cardinality_1_nested' => ['test' => 'normalized field'],
      'field_cardinality_minus1' => ['normalized field'],
    ];
    $this->assertEquals($expected, $normalizer->normalize($content_entity_mock->reveal(), new NormalizerContext()));
  }

  /**
   * Create an instance of ContentEntity.
   *
   * @return \Drupal\lod\Plugin\LodNormalizer\entity\ContentEntity
   *   ContentEntity plugin.
   */
  protected function getContentEntityNormalizer() {
    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize(Argument::any(), 'json_ld', Argument::type('array'))
      ->willReturn('normalized field');

    return new ContentEntity([], '', [], $serializer->reveal());
  }

  /**
   * Creates a mock content entity.
   *
   * @param array $definitions
   *   Array of FieldItemListInterface objects.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked content entity.
   */
  protected function createMockContentEntity(array $definitions) {
    $content_entity_mock = $this->prophesize(ContentEntityInterface::class);

    $typed_data = $this->prophesize(ComplexDataInterface::class);
    $typed_data->getProperties(TRUE)
      ->willReturn($definitions)
      ->shouldBeCalled();
    $content_entity_mock->getTypedData()
      ->willReturn($typed_data)
      ->shouldBeCalled();

    return $content_entity_mock;
  }

  /**
   * Create mocked data definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked data definition.
   */
  protected function createMockDataDefinition() {
    $data_definition = $this->prophesize(FieldDefinitionInterface::class);

    $data_definition->isInternal()
      ->willReturn(FALSE);

    return $data_definition;
  }

}
