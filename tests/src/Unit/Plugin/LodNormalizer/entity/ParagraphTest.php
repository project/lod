<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\entity;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\lod\LodNormalizerPluginInterface;
use Drupal\lod\Plugin\LodNormalizer\entity\Paragraph;
use Drupal\lod\Value\NormalizerContext;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * ParagraphTest unit tests.
 *
 * @group lod
 */
class ParagraphTest extends EntityTestBase {

  /**
   * Test create().
   */
  public function testCreate() {
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $renderer = $this->prophesize(RendererInterface::class);
    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $container = $this->prophesize(ContainerInterface::class);
    $container->get('entity_type.manager')
      ->willReturn($entity_type_manager)
      ->shouldBeCalled();
    $container->get('renderer')
      ->willReturn($renderer)
      ->shouldBeCalled();
    $container->get('language_manager')
      ->willReturn($language_manager)
      ->shouldBeCalled();

    $configuration = [
      'serializer' => new Serializer(),
    ];

    $normalizer = Paragraph::create($container->reveal(), $configuration, '', '');
    $this->assertInstanceOf(LodNormalizerPluginInterface::class, $normalizer);
  }

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $entity_adapter = $this->prophesize(ContentEntityInterface::class);
    $entity_adapter->isTranslatable()
      ->willReturn(TRUE)
      ->shouldbeCalled();

    $paragraph = $this->prophesize(ParagraphInterface::class);
    $paragraph->getParentEntity()
      ->willReturn($entity_adapter)
      ->shouldbeCalled();
    $paragraph->isTranslatable()
      ->willReturn(TRUE)
      ->shouldbeCalled();

    $serializer = $this->prophesize(Serializer::class);

    $view_builder = $this->prophesize(EntityViewBuilderInterface::class);
    $view_builder->view($paragraph, 'full', 'nl')
      ->willReturn(['renderNL'])
      ->shouldbeCalled();
    $view_builder->view($paragraph, 'full', 'es')
      ->willReturn(['renderES'])
      ->shouldbeCalled();

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $entity_type_manager->getViewBuilder('paragraph')
      ->willReturn($view_builder)
      ->shouldBeCalled();

    $markup = $this->prophesize(MarkupInterface::class);
    $markup->jsonSerialize()
      ->willReturn('serializedNL', 'serializedES')
      ->shouldBeCalled();

    $renderer = $this->prophesize(RendererInterface::class);
    $renderer->render(['renderNL'])
      ->willReturn($markup)
      ->shouldBeCalled();
    $renderer->render(['renderES'])
      ->willReturn($markup)
      ->shouldBeCalled();

    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $language_manager->getLanguages()
      ->willReturn(['nl' => 'nl', 'es' => 'es'])
      ->shouldBeCalled();

    $normalizer = new Paragraph([], '', [], $serializer->reveal(), $entity_type_manager->reveal(), $renderer->reveal(), $language_manager->reveal());
    $expected = [
      'nl' => 'serializedNL',
      'es' => 'serializedES',
    ];
    $this->assertEquals($expected, $normalizer->normalize($paragraph->reveal(), new NormalizerContext()));
  }

  /**
   * Test untranslatable normalize().
   */
  public function testUntranslatableNormalize() {
    $paragraph = $this->prophesize(ParagraphInterface::class);
    $paragraph->isTranslatable()
      ->willReturn(FALSE)
      ->shouldbeCalled();

    $serializer = $this->prophesize(Serializer::class);

    $view_builder = $this->prophesize(EntityViewBuilderInterface::class);
    $view_builder->view($paragraph)
      ->willReturn(['render'])
      ->shouldbeCalled();

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $entity_type_manager->getViewBuilder('paragraph')
      ->willReturn($view_builder)
      ->shouldBeCalled();

    $markup = $this->prophesize(MarkupInterface::class);
    $markup->jsonSerialize()
      ->willReturn('serialized')
      ->shouldBeCalled();

    $renderer = $this->prophesize(RendererInterface::class);
    $renderer->render(['render'])
      ->willReturn($markup)
      ->shouldBeCalled();

    $language_manager = $this->prophesize(LanguageManagerInterface::class);

    $normalizer = new Paragraph([], '', [], $serializer->reveal(), $entity_type_manager->reveal(), $renderer->reveal(), $language_manager->reveal());
    $expected = 'serialized';
    $this->assertEquals($expected, $normalizer->normalize($paragraph->reveal(), new NormalizerContext()));
  }

}
