<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\lod\Plugin\LodNormalizer\entity\ContentEntity;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\Serializer\Serializer;

/**
 * Base class for entity normalizer tests.
 *
 * @group lod
 */
abstract class EntityTestBase extends UnitTestCase {

  /**
   * Create an instance of ContentEntity.
   *
   * @return \Drupal\lod\Plugin\LodNormalizer\entity\ContentEntity
   *   ContentEntity plugin.
   */
  protected function getContentEntityNormalizer() {
    $serializer = $this->prophesize(Serializer::class);
    $serializer->normalize(Argument::any(), 'json_ld', Argument::type('array'))
      ->willReturn('normalized field');

    return new ContentEntity([], '', [], $serializer->reveal());
  }

  /**
   * Creates a mock content entity.
   *
   * @param array $definitions
   *   Array of FieldItemListInterface objects.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked content entity.
   */
  protected function createMockContentEntity(array $definitions) {
    $content_entity_mock = $this->prophesize(ContentEntityInterface::class);

    $typed_data = $this->prophesize(ComplexDataInterface::class);
    $typed_data->getProperties(TRUE)
      ->willReturn($definitions)
      ->shouldBeCalled();
    $content_entity_mock->getTypedData()
      ->willReturn($typed_data)
      ->shouldBeCalled();

    return $content_entity_mock;
  }

  /**
   * Creates a mock field list item.
   *
   * @param int $cardinality
   *   The field cardinality.
   * @param mixed $value
   *   The value of the first item.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked field list item.
   */
  protected function createMockFieldListItem($cardinality = -1, $value = 'normalized field') {
    $mock = $this->prophesize(FieldItemListInterface::class);

    $field_definition = $this->prophesize(BaseFieldDefinition::class);
    $field_definition->getCardinality()
      ->willReturn($cardinality);

    $mock->getFieldDefinition()
      ->willReturn($field_definition);

    $data_definition = $this->createMockDataDefinition();
    $mock->getDataDefinition()
      ->willReturn($data_definition);

    if ($cardinality === 1) {
      $mock->first()
        ->willReturn($value);
    }

    return $mock;
  }

  /**
   * Create mocked data definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked data definition.
   */
  protected function createMockDataDefinition() {
    $data_definition = $this->prophesize(FieldDefinitionInterface::class);

    $data_definition->isInternal()
      ->willReturn(FALSE);

    return $data_definition;
  }

}
