<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\entity;

use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\Url;
use Drupal\lod\Encoder\JsonLdEncoder;
use Drupal\lod\Plugin\LodNormalizer\entity\Term;
use Drupal\lod\Value\NormalizerContext;
use Drupal\taxonomy\TermInterface;
use Prophecy\Argument;
use Symfony\Component\Serializer\Serializer;

/**
 * Term units tests.
 *
 * @group lod
 */
class TermTest extends EntityTestBase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $serializer = $this->prophesize(Serializer::class);

    $term = $this->prophesize(TermInterface::class);

    $typed_data = $this->prophesize(ComplexDataInterface::class);
    $typed_data->getProperties(TRUE)
      ->willReturn([
        'parent' => $this->createMockFieldListItem(),
      ])
      ->shouldBeCalled();
    $term->getTypedData()
      ->willReturn($typed_data)
      ->shouldBeCalled();

    $url = $this->prophesize(Url::class);
    $url->toString()
      ->willReturn('url');

    $term->toUrl('canonical', Argument::any())
      ->willReturn($url);
    $term->bundle()
      ->willReturn('bundle');
    $term->id()
      ->willReturn(1);

    $normalizer = new Term([], '', [], $serializer->reveal());

    $expected = [
      'url' => 'url',
      'page' => 'url',
      'vocabulary' => 'bundle',
      'parent' => [],
    ];

    $this->assertEquals($expected, $normalizer->normalize($term->reveal(), new NormalizerContext()));
  }

}
