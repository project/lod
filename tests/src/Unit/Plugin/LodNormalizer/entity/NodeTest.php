<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\entity;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\lod\Plugin\LodNormalizer\entity\Node;
use Drupal\lod\Value\NormalizerContext;
use Symfony\Component\Serializer\Serializer;

/**
 * ContentEntityNormalizer units tests.
 *
 * @group lod
 */
class NodeTest extends EntityTestBase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $serializer = $this->createMock(Serializer::class);
    $serializer->method('normalize')
      ->willReturn('normalized field');

    $language_manager = $this->createMock(LanguageManagerInterface::class);

    $normalizer = new Node([], '', [], $serializer, $language_manager);

    $content_entity_mock = $this->createMockContentEntity([
      'field_1' => $this->createMockFieldListItem(),
      'field_2' => $this->createMockFieldListItem(),
      'nid' => $this->createMockFieldListItem(),
      'path' => $this->createMockFieldListItem(),
    ]);

    $url = $this->prophesize(Url::class);
    $url->toString()
      ->willReturn('url');

    $page = $this->prophesize(Url::class);
    $page->toString()
      ->willReturn('page');

    $content_entity_mock->toUrl('canonical', ['absolute' => TRUE])
      ->willReturn($url);

    $content_entity_mock->toUrl('canonical', ['absolute' => TRUE, 'path_processing' => FALSE])
      ->willReturn($page);

    $context = new NormalizerContext();
    $context->addExcludedField('field_2');
    $context->addFieldTranslation('field_1', 'renamed_field_1');

    $normalized = $normalizer->normalize($content_entity_mock->reveal(), $context);

    $this->assertArrayHasKey('renamed_field_1', $normalized);
    $this->assertArrayNotHasKey('field_2', $normalized);
    $this->assertArrayNotHasKey('nid', $normalized);
    $this->assertArrayNotHasKey('path', $normalized);
    $this->assertEquals('url', $normalized['url']);
    $this->assertEquals('page', $normalized['page']);
  }

}
