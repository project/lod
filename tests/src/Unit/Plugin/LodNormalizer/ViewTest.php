<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer;

use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\lod\LodNormalizerPluginInterface;
use Drupal\lod\Plugin\LodNormalizer\View;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Drupal\views\Plugin\views\pager\PagerPluginBase;
use Drupal\views\Plugin\views\row\RowPluginBase;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * View units tests.
 *
 * @group lod
 */
class ViewTest extends UnitTestCase {

  /**
   * Test create().
   */
  public function testCreate() {
    $urlGenerator = $this->createMock(UrlGeneratorInterface::class);
    $container = $this->prophesize(ContainerInterface::class);
    $container->get('url_generator')
      ->willReturn($urlGenerator)
      ->shouldBeCalled();

    $configuration = [
      'serializer' => new Serializer(),
    ];

    $normalizer = View::create($container->reveal(), $configuration, '', '');
    $this->assertInstanceOf(LodNormalizerPluginInterface::class, $normalizer);
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithoutFullPager() {
    $serializer = $this->createMock(Serializer::class);
    $serializer->method('normalize')
      ->will($this->returnArgument(0));

    $urlGenerator = $this->createMock(UrlGeneratorInterface::class);

    $pager = $this->createPagerMock('someOtherPluginId');

    $view = $this->createViewsExecutableMock();
    $view->method('getPager')
      ->willReturn($pager);
    $view->method('getExposedInput')
      ->willReturn($this->createExposedInput());

    $normalizer = new View([], '', [], $serializer, $urlGenerator);
    $normalized = $normalizer->normalize($view, new NormalizerContext());
    $this->assertCount(2, $normalized['member']);
  }

  /**
   * Test normalize().
   */
  public function testNormalizeWithFullPager() {
    $serializer = $this->createMock(Serializer::class);
    $serializer->method('normalize')
      ->will($this->returnArgument(0));

    $urlGenerator = $this->createMock(UrlGeneratorInterface::class);

    $pager = $this->createPagerMock();

    $view = $this->createViewsExecutableMock();
    $view->method('getPager')
      ->willReturn($pager);
    $view->method('getExposedInput')
      ->willReturn($this->createExposedInput());

    $normalizer = new View([], '', [], $serializer, $urlGenerator);
    $normalized = $normalizer->normalize($view, new NormalizerContext());

    $this->assertArrayHasKey('firstPage', $normalized);
    $this->assertArrayHasKey('previousPage', $normalized);
    $this->assertArrayHasKey('nextPage', $normalized);
    $this->assertArrayHasKey('lastPage', $normalized);
  }

  /**
   * Test normalize().
   */
  public function testNormalizeFirstPage() {
    $serializer = $this->createMock(Serializer::class);
    $serializer->method('normalize')
      ->will($this->returnArgument(0));

    $urlGenerator = $this->createMock(UrlGeneratorInterface::class);

    $pager = $this->createPagerMock('full', 0);

    $view = $this->createViewsExecutableMock();
    $view->method('getPager')
      ->willReturn($pager);
    $view->method('getExposedInput')
      ->willReturn($this->createExposedInput());

    $normalizer = new View([], '', [], $serializer, $urlGenerator);
    $normalized = $normalizer->normalize($view, new NormalizerContext());

    $this->assertArrayHasKey('firstPage', $normalized);
    $this->assertArrayNotHasKey('previousPage', $normalized);
    $this->assertArrayHasKey('nextPage', $normalized);
    $this->assertArrayHasKey('lastPage', $normalized);
  }

  /**
   * Test normalize().
   */
  public function testNormalizeLastPage() {
    $serializer = $this->createMock(Serializer::class);
    $serializer->method('normalize')
      ->will($this->returnArgument(0));

    $urlGenerator = $this->createMock(UrlGeneratorInterface::class);

    $pager = $this->createPagerMock('full', 3, FALSE);

    $view = $this->createViewsExecutableMock();
    $view->method('getPager')
      ->willReturn($pager);
    $view->method('getExposedInput')
      ->willReturn($this->createExposedInput());

    $normalizer = new View([], '', [], $serializer, $urlGenerator);
    $normalized = $normalizer->normalize($view, new NormalizerContext());

    $this->assertArrayHasKey('firstPage', $normalized);
    $this->assertArrayHasKey('previousPage', $normalized);
    $this->assertArrayNotHasKey('nextPage', $normalized);
    $this->assertArrayHasKey('lastPage', $normalized);
  }

  /**
   * Return a ViewExecutableMock.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject|ViewExecutable
   */
  protected function createViewsExecutableMock() {
    $rowPlugin = $this->createMock(RowPluginBase::class);
    $rowPlugin->method('render')
      ->willReturn($this->returnArgument(0));

    $view = $this->createMock(ViewExecutable::class);

    $view->rowPlugin = $rowPlugin;

    $view->result = [
      'row1',
      'row2',
    ];

    return $view;
  }

  protected function createPagerMock($type = 'full', $currentPage = 3, $hasMoreRecords = TRUE, $totalItems = 22, $itemsPerPage = 5) {
    $pager = $this->createMock(PagerPluginBase::class);

    $pager->method('getPluginId')
      ->willReturn($type);

    $pager->method('getCurrentPage')
      ->willReturn($currentPage);

    $pager->method('hasMoreRecords')
      ->willReturn($hasMoreRecords);

    $pager->method('getTotalItems')
      ->willReturn($totalItems);

    $pager->method('getItemsPerPage')
      ->willReturn($itemsPerPage);

    return $pager;
  }

  protected function createExposedInput() {
    return ['modified' => '20191028'];
  }
}
