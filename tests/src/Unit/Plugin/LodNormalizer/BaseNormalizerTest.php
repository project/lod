<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer;

use Drupal\lod\LodNormalizerPluginInterface;
use Drupal\Tests\lod\Unit\Plugin\LodNormalizer\Subclasses\Test;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * BaseNormalizer unit tests.
 *
 * @group lod
 */
class BaseNormalizerTest extends UnitTestCase {

  /**
   * Test create().
   */
  public function testCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $configuration = [
      'serializer' => new Serializer(),
    ];

    $normalizer = Test::create($container, $configuration, '', '');
    $this->assertInstanceOf(LodNormalizerPluginInterface::class, $normalizer);
  }

  /**
   * Test supportsNormalization().
   */
  public function testSupportsNormalizationIsAlwaysTrue() {
    $normalizer = new Test([], '', [], new Serializer());
    $this->assertTrue($normalizer->supportsNormalization(new \stdClass()));
  }

}
