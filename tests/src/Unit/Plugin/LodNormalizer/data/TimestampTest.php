<?php

namespace Drupal\Tests\lod\Unit\Plugin\LodNormalizer\data;

use Drupal\lod\Plugin\LodNormalizer\data\Timestamp;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Serializer\Serializer;

/**
 * ContentEntityNormalizer units tests.
 *
 * @group lod
 */
class TimestampTest extends UnitTestCase {

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $normalizer = new Timestamp([], '', [], new Serializer());
    $object = $this->prophesize('Drupal\Core\TypedData\Plugin\DataType\Timestamp');
    $object->getCastedValue()
      ->willReturn(1521532082);
    $this->assertEquals('2018-03-20T07:48:02+00:00', $normalizer->normalize($object->reveal(), new NormalizerContext()));
  }

}
