<?php

namespace Drupal\Tests\lod\Unit\Plugin\views\Subclasses;

use Drupal\Core\Render\RendererInterface;
use Drupal\lod\Plugin\views\display\JsonLdExport;
use Prophecy\Prophet;

/**
 * Class to TestJsonLdExport.
 */
class TestJsonLdExport extends JsonLdExport {

  /**
   * Allow us to mock the plugins.
   *
   * @param array $plugins
   *   Value for $this->plugins.
   */
  public function setPlugins(array $plugins) {
    $this->plugins = $plugins;
  }

  /**
   * Avoid calling parent::optionsSummary().
   */
  protected function parentOptionsSummary(array &$categories, array &$options) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  protected static function getStaticRenderer() {
    $prophet = new Prophet();
    return $prophet->prophesize(RendererInterface::class)->reveal();
  }

}
