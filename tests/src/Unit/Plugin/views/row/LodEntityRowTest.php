<?php

namespace Drupal\Tests\lod\Unit\Plugin\views\display;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityType;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\lod\Plugin\views\display\JsonLdExport;
use Drupal\lod\Plugin\views\row\LodEntityRow;
use Drupal\Tests\UnitTestCase;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\Plugin\views\ViewsPluginInterface;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @covers \Drupal\lod\Plugin\views\row\LodEntityRow
 *
 * @group lod
 */
class LodEntityRowTest extends UnitTestCase {

  /**
   * Test create().
   */
  public function testCreate() {
    $container = $this->prophesize(ContainerInterface::class);
    $entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $entityRepository = $this->prophesize(EntityRepositoryInterface::class);
    $language_manager = $this->prophesize(LanguageManagerInterface::class);
    $container->get('entity_type.manager')
      ->willReturn($entityTypeManager)
      ->shouldBeCalledTimes(1);
    $container->get('entity.repository')
      ->willReturn($entityRepository)
      ->shouldBeCalledTimes(1);
    $container->get('language_manager')
      ->willReturn($language_manager)
      ->shouldBeCalledTimes(1);

    $plugin = LodEntityRow::create($container->reveal(), [], '', []);
    $this->assertInstanceOf(ViewsPluginInterface::class, $plugin);
  }

  /**
   * Test render().
   */
  public function testRender() {
    $plugin = $this->getLodEntityRow();
    $entity = $this->prophesize(EntityInterface::class);
    $row = $this->prophesize(ResultRow::class);
    $row->_entity = $entity->reveal();

    $result = $plugin->render($row->reveal());
    $this->assertEquals($entity->reveal(), $result);
  }

  /**
   * Test getEntityTypeId().
   */
  public function testGetEntityTypeId() {
    $plugin = $this->getLodEntityRow();
    $view = $this->createViewMock();
    $plugin->view = $view->reveal();

    $this->assertEquals('id', $plugin->getEntityTypeId());
  }

  /**
   * Test query().
   */
  public function testQuery() {
    $plugin = $this->getLodEntityRow();
    $view = $this->createViewMock();

    $plugin->view = $view->reveal();

    $view->getQuery()
      ->willReturn($this->prophesize(QueryPluginBase::class))
      ->shouldBeCalledTimes(1);

    $plugin->query();
  }

  /**
   * Create a JsonLdExport object.
   *
   * @return \Drupal\lod\Plugin\views\row\LodEntityRow
   *   The plugin.
   */
  protected function getLodEntityRow() {
    $entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $entityTypeManager
      ->getDefinition(Argument::any())
      ->willReturn($this->prophesize(EntityTypeInterface::class));
    $entityRepository = $this->prophesize(EntityRepositoryInterface::class);
    $language_manager = $this->prophesize(LanguageManagerInterface::class);

    return new LodEntityRow(
      [],
      '',
      [],
      $entityTypeManager->reveal(),
      $entityRepository->reveal(),
      $language_manager->reveal()
    );
  }

  /**
   * Get mocked view executable.
   *
   * @return \Drupal\views\ViewExecutable|\Prophecy\Prophecy\ObjectProphecy
   *   Mocked view executable.
   */
  protected function createViewMock() {
    $view = $this->prophesize(ViewExecutable::class);

    $entity_type = $this->prophesize(EntityType::class);
    $entity_type->id()
      ->willReturn('id');
    $view->getBaseEntityType()
      ->willReturn($entity_type);

    $display_handler = $this->prophesize(JsonLdExport::class);
    $display_handler->getOption('rendering_language')
      ->willReturn('rendering_language');
    $view->display_handler = $display_handler->reveal();

    return $view;
  }

}
