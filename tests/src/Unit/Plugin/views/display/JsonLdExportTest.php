<?php

namespace Drupal\Tests\lod\Unit\Plugin\views\display;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\State\StateInterface;
use Drupal\lod\Plugin\views\display\JsonLdExport;
use Drupal\Tests\lod\Unit\Plugin\views\Subclasses\TestJsonLdExport;
use Drupal\Tests\UnitTestCase;
use Drupal\views\Entity\View;
use Drupal\views\Plugin\views\access\AccessPluginBase;
use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\display\ResponseDisplayPluginInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\ViewExecutable;
use Prophecy\Argument;
use Prophecy\Prophecy\MethodProphecy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * BaseNormalizer unit tests.
 *
 * @group lod
 */
class JsonLdExportTest extends UnitTestCase {

  /**
   * Test create().
   */
  public function testCreate() {
    $plugin = $this->getJsonLdExport();
    $this->assertInstanceOf(ResponseDisplayPluginInterface::class, $plugin);
  }

  /**
   * Test settings.
   */
  public function testSettings() {
    $plugin = $this->getJsonLdExport();
    $this->assertEquals('lod', $plugin->getType());
    $this->assertTrue($plugin->usesExposed());
    $this->assertFalse($plugin->displaysExposed());
  }

  /**
   * Test execute().
   */
  public function testExecute() {
    $plugin = $this->getJsonLdExport();
    $view = $this->prophesize(ViewExecutable::class);
    $view->render()
      ->willReturn([])
      ->shouldBeCalled();
    $view->build()
      ->willReturn(TRUE)
      ->shouldBeCalled();
    $plugin->view = $view->reveal();
    $plugin->execute();
  }

  /**
   * Test preview().
   */
  public function testPreview() {
    $plugin = $this->getJsonLdExport();
    $view = $this->prophesize(ViewExecutable::class);
    $view->render()
      ->willReturn([])
      ->shouldBeCalled();

    $plugin->view = $view->reveal();
    $plugin->preview();
  }

  /**
   * Test defineOptions().
   */
  public function testDefineOptions() {
    $plugin = $this->getJsonLdExport();
    $view = $this->prophesize(ViewExecutable::class);
    $display_plugin = $this->prophesize(DisplayPluginBase::class);
    // Calling init, defineOptions is protected.
    $plugin->init($view->reveal(), $display_plugin->reveal());

    $options = $plugin->options;
    $this->assertEquals($options['style']['type'], 'json_ld_serializer');
    $this->assertEquals($options['row']['type'], 'lod_entity');
    $this->assertFalse($options['defaults']['style']);
    $this->assertFalse($options['defaults']['row']);

    $this->assertArrayNotHasKey('exposed_form', $options);
    $this->assertArrayNotHasKey('exposed_block', $options);
    $this->assertArrayNotHasKey('css_class', $options);
    $this->assertArrayHasKey('rendering_language', $options);
  }

  /**
   * Test collectRoutes().
   */
  public function testCollectRoutes() {
    $plugin = TestJsonLdExport::create($this->createContainerMock()->reveal(), [], '', []);

    $view = $this->prophesize(ViewExecutable::class);
    $view_storage = $this->prophesize(View::class);
    $view_storage->id()
      ->willReturn('view_id');
    $view->storage = $view_storage->reveal();
    $plugin->view = $view->reveal();
    $plugin->display['id'] = 'display_id';

    $route_collection = $this->prophesize(RouteCollection::class);
    $route_collection->add(Argument::any(), Argument::any())
      ->shouldBeCalled();

    $route = $this->prophesize(Route::class);
    $route->setMethods(['GET'])
      ->shouldBeCalled();
    $route_collection->get('view.view_id.display_id')
      ->willReturn($route);

    // Mock access plugin.
    $plugin->options['access'] = [
      'type' => 'access',
    ];
    $access_plugin = $this->prophesize(AccessPluginBase::class);
    $access_plugin->alterRouteDefinition(Argument::any());
    $plugin->setPlugins([
      'access' => ['access' => $access_plugin->reveal()],
    ]);

    $plugin->collectRoutes($route_collection->reveal());
  }

  /**
   * Test optionsSummary().
   */
  public function testOptionsSummary() {
    $plugin = TestJsonLdExport::create($this->createContainerMock()->reveal(), [], '', []);
    $plugin->setStringTranslation($this->getStringTranslationStub());

    $categories = [
      'page',
      'exposed',
    ];
    $options = [
      'show_admin_links',
      'analyze-theme',
      'exposed_form',
      'exposed_block',
      'css_class',
    ];

    $plugin->optionsSummary($categories, $options);
    $this->assertArrayNotHasKey('exposed', $categories);
    $this->assertArrayNotHasKey('analyze-theme', $options);
    $this->assertArrayNotHasKey('exposed_form', $options);
    $this->assertArrayNotHasKey('exposed_block', $options);
    $this->assertArrayNotHasKey('css_class', $options);
  }

  /**
   * Test buildResponse().
   */
  public function testbuildResponse() {
    $response = TestJsonLdExport::buildResponse('view_id', 'display_id');
    $this->assertInstanceOf(CacheableResponseInterface::class, $response);
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertEquals('application/ld+json', $response->headers->get('Content-type'));
  }

  /**
   * Test render().
   */
  public function testRender() {
    $plugin = TestJsonLdExport::create($this->createContainerMock()->reveal(), [], '', []);
    $view = $this->prophesize(ViewExecutable::class);
    $view->getCacheTags()
      ->willReturn([]);
    $plugin->view = $view->reveal();

    $this->addCachePlugin($plugin);

    $this->assertArrayHasKey('#markup', $plugin->render());
  }

  /**
   * Test render().
   */
  public function testRenderLivePreview() {
    $plugin = TestJsonLdExport::create($this->createContainerMock()->reveal(), [], '', []);
    $view = $this->prophesize(ViewExecutable::class);
    $view->getCacheTags()
      ->willReturn([]);
    $view->live_preview = TRUE;
    $plugin->view = $view->reveal();

    $this->addCachePlugin($plugin);

    $build = $plugin->render();
    $this->assertArrayNotHasKey('#markup', $build);
    $this->assertArrayHasKey('#plain_text', $build);
  }

  /**
   * Test render().
   */
  public function testRenderWithStylePluginReturn() {
    $container = $this->prophesize(ContainerInterface::class);

    $container->get('router.route_provider')
      ->willReturn($this->prophesize(RouteProviderInterface::class));
    $container->get('state')
      ->willReturn($this->prophesize(StateInterface::class));

    $renderer = $this->prophesize(RendererInterface::class);
    $renderer->executeInRenderContext(Argument::type(RenderContext::class), Argument::any())
      ->will(function ($args) {
        return call_user_func($args[1]);
      });
    $container->get('renderer')
      ->willReturn($renderer);

    $plugin = TestJsonLdExport::create($container->reveal(), [], '', []);
    $view = $this->prophesize(ViewExecutable::class);
    $view->getCacheTags()
      ->willReturn([]);

    $style_plugin = $this->prophesize(StylePluginBase::class);
    $style_plugin->render()
      ->willReturn('style_plugin_render');
    $view->style_plugin = $style_plugin->reveal();

    $plugin->view = $view->reveal();

    $this->addCachePlugin($plugin);

    $this->assertEquals('style_plugin_render', $plugin->render()['#markup']);
  }

  /**
   * Add mocked cache plugin.
   *
   * @param \Drupal\Tests\lod\Unit\Plugin\views\Subclasses\TestJsonLdExport $plugin
   *   TestJsonLdExport where the cache plugin will be added to.
   */
  protected function addCachePlugin(TestJsonLdExport $plugin) {
    // Mock cache plugin.
    $plugin->options['cache'] = [
      'type' => 'cache',
    ];
    $cache_plugin = $this->prophesize(CachePluginBase::class);
    $cache_plugin->getCacheMaxAge()
      ->willReturn(Cache::PERMANENT);
    $plugin->setPlugins([
      'cache' => ['cache' => $cache_plugin->reveal()],
    ]);
  }

  /**
   * Create a JsonLdExport object.
   *
   * @return \Drupal\lod\Plugin\views\display\JsonLdExport
   *   The plugin.
   */
  protected function getJsonLdExport() {
    return JsonLdExport::create($this->createContainerMock()->reveal(), [], '', []);
  }

  /**
   * Create a mocked container.
   *
   * @return \Prophecy\Prophecy\ObjectProphecy
   *   Mocked container.
   */
  protected function createContainerMock() {
    $container = $this->prophesize(ContainerInterface::class);
    $container->get('router.route_provider')
      ->willReturn($this->prophesize(RouteProviderInterface::class));
    $container->get('state')
      ->willReturn($this->prophesize(StateInterface::class));
    $container->get('renderer')
      ->willReturn($this->prophesize(RendererInterface::class));

    return $container;
  }

}
