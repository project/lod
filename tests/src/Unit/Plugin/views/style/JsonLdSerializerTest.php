<?php

namespace Drupal\Tests\lod\Unit\Plugin\views\display;

use Drupal\Core\Cache\Cache;
use Drupal\lod\Plugin\views\style\JsonLdSerializer;
use Drupal\Tests\UnitTestCase;
use Drupal\views\Plugin\views\row\RowPluginBase;
use Drupal\views\Plugin\views\ViewsPluginInterface;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * JsonLdSerializer unit tests.
 *
 * @group lod
 */
class JsonLdSerializerTest extends UnitTestCase {

  /**
   * Test create().
   */
  public function testCreate() {
    $container = $this->prophesize(ContainerInterface::class);
    $serializer = $this->prophesize(SerializerInterface::class);
    $container->get('serializer')
      ->willReturn($serializer)
      ->shouldBeCalledTimes(1);
    $container->getParameter('lod_debug')
      ->willReturn(FALSE)
      ->shouldBeCalledTimes(1);

    $plugin = JsonLdSerializer::create($container->reveal(), [], '', []);
    $this->assertInstanceOf(ViewsPluginInterface::class, $plugin);
    // Settings.
    $this->assertEmpty($plugin->getCacheContexts());
    $this->assertEmpty($plugin->getCacheTags());
    $this->assertEquals(Cache::PERMANENT, $plugin->getCacheMaxAge());
  }

  /**
   * Test calculateDependencies()
   */
  public function testCalculateDependencies() {
    $plugin = $this->getJsonLdSerializer();
    $dependencies = $plugin->calculateDependencies();
    $this->assertTrue(\in_array('serialization', $dependencies['module'], TRUE));
  }

  /**
   * Test render().
   */
  public function testRenderWithDebug() {
    $serializer = $this->prophesize(SerializerInterface::class);
    $plugin = new JsonLdSerializer([], '', [], $serializer->reveal(), TRUE);
    $view = $this->prophesize(ViewExecutable::class);
    $plugin->view = $view;
    $expected_context = [
      'json_encode_options' => JSON_PRETTY_PRINT,
    ];

    $serializer->serialize($view, 'json_ld', $expected_context)
      ->shouldBeCalledTimes(1);

    $plugin->render();
  }

  /**
   * Test render().
   */
  public function testRender() {
    $serializer = $this->prophesize(SerializerInterface::class);

    $plugin = new JsonLdSerializer([], '', [], $serializer->reveal(), FALSE);
    $view = $this->prophesize(ViewExecutable::class);
    $plugin->view = $view;

    $serializer->serialize($view, 'json_ld', [])
      ->shouldBeCalledTimes(1);

    $plugin->render();
  }

  /**
   * Get JsonLdSerializer instance.
   *
   * @param bool $debug
   *   Initiate in debug mode, defaults to FALSE.
   *
   * @return \Drupal\lod\Plugin\views\style\JsonLdSerializer
   *   The JsonLdSerializer instance.
   */
  protected function getJsonLdSerializer($debug = FALSE) {
    $serializer = $this->prophesize(SerializerInterface::class);

    return new JsonLdSerializer([], '', [], $serializer->reveal(), $debug);
  }

}
