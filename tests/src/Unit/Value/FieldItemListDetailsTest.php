<?php

namespace Drupal\Tests\lod\Unit\Value;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\lod\Value\FieldItemListDetails;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Drupal\Tests\UnitTestCase;

/**
 * @covers \Drupal\lod\Value\FieldItemListDetails
 *
 * @group lod
 */
class FieldItemListDetailsTest extends UnitTestCase {

  /**
   * Cardinality retrieved from definition if is BaseFieldDefinition.
   *
   * @test
   */
  public function cardinalityIsRetrievedFromFieldDefinitionIfItIsBaseFieldDefinition(): void {
    $fieldDefinition = $this->prophesize(BaseFieldDefinition::class);
    $fieldDefinition->getCardinality()->willReturn(5)->shouldBeCalled();

    $field = $this->prophesize(FieldItemListInterface::class);
    $field->getFieldDefinition()->willReturn($fieldDefinition->reveal());

    $fieldItemListDetails = new FieldItemListDetails($field->reveal());
    $this->assertSame(5, $fieldItemListDetails->getCardinality());
  }

  /**
   * Cardinality retrieved from field storage definition if it is FieldConfig.
   *
   * @test
   */
  public function cardinalityIsRetrievedFromFieldStorageDefinitionIfDefinitionIsFieldConfig(): void {
    $fieldStorageDefinition = $this->prophesize(FieldStorageConfig::class);
    $fieldStorageDefinition->get('cardinality')->willReturn(10)->shouldBeCalled();

    $fieldDefinition = $this->prophesize(FieldConfig::class);
    $fieldDefinition->getFieldStorageDefinition()->willReturn($fieldStorageDefinition->reveal());

    $field = $this->prophesize(FieldItemListInterface::class);
    $field->getFieldDefinition()->willReturn($fieldDefinition->reveal());

    $fieldItemListDetails = new FieldItemListDetails($field->reveal());
    $this->assertSame(10, $fieldItemListDetails->getCardinality());
  }

  /**
   * Cardinality is 1 if definition is not BaseFieldDefinition or FieldConfig.
   *
   * @test
   */
  public function cadrinalityIsOneIfFieldStorageDefinitionIsNotBaseFieldDefinitionOrFieldConfig(): void {
    $fieldDefinition = $this->prophesize(FieldConfigInterface::class);

    $field = $this->prophesize(FieldItemListInterface::class);
    $field->getFieldDefinition()->willReturn($fieldDefinition->reveal());

    $fieldItemListDetails = new FieldItemListDetails($field->reveal());
    $this->assertSame(1, $fieldItemListDetails->getCardinality());
  }

  /**
   * Field is multi value if the cardinality is not 0.
   *
   * @param int $cardinality
   *   The cardianlity of the field.
   * @param bool $expectedIsMultiValue
   *   Expected isMultiValue() method result.
   *
   * @dataProvider isMultiValueProvider
   *
   * @test
   */
  public function fieldIsMultiValueIfCardinalityIsNotOne(
    int $cardinality,
    bool $expectedIsMultiValue
  ): void {
    $fieldDefinition = $this->prophesize(BaseFieldDefinition::class);
    $fieldDefinition->getCardinality()->willReturn($cardinality);

    $field = $this->prophesize(FieldItemListInterface::class);
    $field->getFieldDefinition()->willReturn($fieldDefinition->reveal());

    $fieldItemListDetails = new FieldItemListDetails($field->reveal());

    $this->assertSame($expectedIsMultiValue, $fieldItemListDetails->isMultiValue());
  }

  /**
   * Data provider to test the isMultiValue() method.
   *
   * @return array
   *   Array containing rows with:
   *   - cardinality (int)
   *   - expectedIsMultiValue (bool)
   */
  public function isMultiValueProvider(): array {
    return [
      '-1 is multi value (unlimited)' => [-1, TRUE],
      '0 is multi value (unlimited)' => [0, TRUE],
      '1 is not multi value' => [1, FALSE],
      '2 is multi value' => [2, TRUE],
    ];
  }

}
