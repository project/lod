<?php

namespace Drupal\Tests\lod\Unit\Value;

use Drupal\Core\Session\AccountInterface;
use Drupal\lod\Value\NormalizerContext;
use Drupal\Tests\UnitTestCase;

/**
 * JsonLdNormalizerTest units tests.
 *
 * @group lod
 */
class NormalizerContextTest extends UnitTestCase {

  /**
   * Test addExcludedField().
   */
  public function testAddExcludedField() {
    $context = new NormalizerContext();
    $context->addExcludedField('test');
    $this->assertEquals(['test'], $context['excluded_field']);
  }

  /**
   * Test getExcludedFields().
   */
  public function testGetExcludedFields() {
    $context = new NormalizerContext(['excluded_field' => ['test']]);
    $this->assertEquals(['test'], $context->getExcludedFields());
  }

  /**
   * Test addExcludedField().
   */
  public function testAddIncludedField() {
    $context = new NormalizerContext();
    $context->addIncludedField('test');
    $this->assertEquals(['test'], $context['included_field']);
  }

  /**
   * Test getExcludedFields().
   */
  public function testGetIncludedFields() {
    $context = new NormalizerContext(['included_field' => ['test']]);
    $this->assertEquals(['test'], $context->getIncludedFields());
  }

  /**
   * Test addExcludedField().
   */
  public function testAddExcludedEntity() {
    $context = new NormalizerContext();
    $context->addExcludedEntityType('test', ['bundle']);
    $this->assertEquals(['test' => ['bundle']], $context['excluded_entity_type']);
  }

  /**
   * Test getExcludedFields().
   */
  public function testGetExcludedEntities() {
    $context = new NormalizerContext(['excluded_entity_type' => ['test' => ['bundle']]]);
    $this->assertEquals(['test' => ['bundle']], $context->getExcludedEntityTypes());
  }

  /**
   * Test addFieldTranslation().
   */
  public function testAddFieldTranslation() {
    $context = new NormalizerContext();
    $context->addFieldTranslation('name', 'renamed');
    $this->assertEquals(['name' => 'renamed'], $context['field_translation']);
  }

  /**
   * Test getFieldTranslations().
   */
  public function testGetFieldTranslations() {
    $context = new NormalizerContext(['field_translation' => ['name' => 'renamed']]);
    $this->assertEquals(['name' => 'renamed'], $context->getFieldTranslations());
  }

  /**
   * Test addReferenceField().
   */
  public function testAddReferenceField() {
    $context = new NormalizerContext();
    $context->addReferenceField('name', 'namespace');
    $this->assertEquals(['name' => 'namespace'], $context['reference_field']);
  }

  /**
   * Test getReferenceFields().
   */
  public function testGetReferenceFields() {
    $context = new NormalizerContext(['reference_field' => ['name' => 'namespace']]);
    $this->assertEquals(['name' => 'namespace'], $context->getReferenceFields());
  }

  /**
   * Test setAccount().
   */
  public function testSetAccount() {
    $context = new NormalizerContext();
    $account = $this->prophesize(AccountInterface::class);
    $context->setAccount($account->reveal());
    $this->assertEquals($account->reveal(), $context['account']);
  }

  /**
   * Test getAccount().
   */
  public function testGetAccount() {
    $account = $this->prophesize(AccountInterface::class);
    $context = new NormalizerContext(['account' => $account->reveal()]);
    $this->assertEquals($account->reveal(), $context->getAccount());
  }

  /**
   * Test setNamespace().
   */
  public function testSetNamespace() {
    $context = new NormalizerContext();
    $context->setNamespace('namespace');
    $this->assertEquals('namespace', $context['namespace']);
  }

  /**
   * Test getNamespace().
   */
  public function testGetNamespace() {
    $context = new NormalizerContext(['namespace' => 'namespace']);
    $this->assertEquals('namespace', $context->getNamespace());
  }

  /**
   * Test getValue().
   */
  public function testGetValue() {
    $context = new NormalizerContext();
    $this->assertNull($context->getNamespace());
  }

  /**
   * Test getArrayValue().
   */
  public function testGetArrayValue() {
    $context = new NormalizerContext();
    $this->assertIsArray($context->getExcludedFields());
    $this->assertEmpty($context->getExcludedFields());
  }

  /**
   * Test addArrayValue().
   */
  public function testAddArrayValue() {
    $context = new NormalizerContext();
    $context->addExcludedField('test');
    $context->addExcludedField('test');
    $this->assertEquals(['test'], $context->getExcludedFields(), 'No duplicates in return');
  }

}
