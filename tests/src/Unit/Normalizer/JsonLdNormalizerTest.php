<?php

namespace Drupal\Tests\lod\Unit\Normalizer;

use Drupal\lod\LodNormalizerPluginInterface;
use Drupal\lod\LodNormalizerManager;
use Drupal\lod\Normalizer\JsonLdNormalizer;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Serializer\Serializer;

/**
 * JsonLdNormalizerTest units tests.
 *
 * @group lod
 */
class JsonLdNormalizerTest extends UnitTestCase {

  /**
   * Test supportNormalization() if format is not json_ld.
   */
  public function testSupportNormalizationNotIfFormatIsNotJsonLd() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager());

    $this->assertFalse($normalizer->supportsNormalization(new \stdClass(), 'not-json_lod'), 'Support only JSON-LD');
  }

  /**
   * Test supportNormalization() if data is not an object.
   */
  public function testSupportNormalizationNotIfNotObject() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager());

    $this->assertFalse($normalizer->supportsNormalization('a string is not an object', 'json_ld'), 'Support only objects');
  }

  /**
   * Test supportNormalization() if no plugins are defined.
   */
  public function testSupportNormalizationNotIfNoPlugins() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager([]));

    $this->assertFalse($normalizer->supportsNormalization(new \stdClass(), 'json_ld'), 'No plugins');
  }

  /**
   * Test supportNormalization() if no plugin supports normalization.
   */
  public function testSupportNormalizationNotIfNoSupportedPlugins() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager([
      [
        'id' => 'not supported plugin',
        'supportsNormalization' => FALSE,
      ],
    ]));

    $this->assertFalse($normalizer->supportsNormalization(new \stdClass(), 'json_ld'), 'No supported plugins');
  }

  /**
   * Test supportNormalization() if a plugin supports normalization.
   */
  public function testSupportNormalizationNotIfSupportedPlugins() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager([
      [
        'id' => 'a supported plugin',
        'supportsNormalization' => TRUE,
      ],
    ]));

    $this->assertTrue($normalizer->supportsNormalization(new \stdClass(), 'json_ld'), 'Supported plugin');
  }

  /**
   * Test supportNormalization() if a plugin does not support the class.
   */
  public function testSupportNormalizationNotIfNoPluginWithDifferentClass() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager([
      [
        'id' => 'not supported plugin',
        'supportsNormalization' => TRUE,
        'supportedClass' => 'UnknownClass',
      ],
    ]));

    $this->assertFalse($normalizer->supportsNormalization(new \stdClass(), 'json_ld'), 'No supported plugins');
  }

  /**
   * Test normalize().
   */
  public function testNormalize() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager([
      [
        'id' => 'a supported plugin',
        'supportsNormalization' => TRUE,
        'normalize' => 'test',
      ],
    ]));

    $this->assertSame('test', $normalizer->normalize(new \stdClass()));
  }

  /**
   * Test normalize() with debug.
   */
  public function testNormalizeDebug() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager([
      [
        'id' => 'a supported plugin',
        'supportsNormalization' => TRUE,
        'normalize' => 'test',
      ],
    ]), TRUE);

    $output = $normalizer->normalize(new \stdClass());

    $this->assertSame('test', $output['original_value']);
    $this->assertSame('stdClass', $output['@debug']['objectClass']);
    $this->assertNull($output['@debug']['child']);
  }

  /**
   * Test normalize() with debug and parent debug value.
   */
  public function testNormalizeDebugWithParent() {
    $normalizer = new JsonLdNormalizer($this->getMockedPluginManager([
      [
        'id' => 'a supported plugin',
        'supportsNormalization' => TRUE,
        'normalize' => [
          'test' => 'test',
          '@debug' => 'childDebug',
        ],
      ],
    ]), TRUE);

    $output = $normalizer->normalize(new \stdClass());

    $this->assertSame('childDebug', $output['@debug']['child']);
  }

  /**
   * Test setSerializer().
   */
  public function testSetSerializer() {
    $manager = $this->getMockedPluginManager([['id' => 'test']]);

    $serializer = new Serializer();
    $manager->expects($this->once())
      ->method('createInstance')
      ->with('test', ['serializer' => $serializer]);

    $normalizer = new JsonLdNormalizer($manager);
    $normalizer->setSerializer(new Serializer());

    $normalizer->supportsNormalization(new \stdClass(), 'json_ld');
  }

  /**
   * Get a mocked plugin manager.
   *
   * @param array $plugins
   *   Plugins returned by the plugin manager.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject|\Drupal\lod\LodNormalizerManager
   *   The mocked LodNormalizerManger.
   */
  protected function getMockedPluginManager(array $plugins = []) {
    $manager = $this->createMock(LodNormalizerManager::class);
    $manager->method('getDefinitionsByFormat')
      ->willReturn($plugins);

    foreach ($plugins as $plugin) {
      $mockedPlugin = $this->createMock(LodNormalizerPluginInterface::class);

      if (isset($plugin['supportsNormalization'])) {
        $mockedPlugin->method('supportsNormalization')
          ->willReturn($plugin['supportsNormalization']);
      }

      if (isset($plugin['normalize'])) {
        $mockedPlugin->method('normalize')
          ->willReturn($plugin['normalize']);
      }

      $manager->method('createInstance')
        ->with($plugin['id'])
        ->willReturn($mockedPlugin);
    }

    return $manager;
  }

}
