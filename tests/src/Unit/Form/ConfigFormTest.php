<?php

namespace src\Unit\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lod\Form\ConfigForm;
use Drupal\Tests\UnitTestCase;

/**
 * @covers \Drupal\lod\Form\ConfigForm
 */
class ConfigFormTest extends UnitTestCase {

  /**
   * Check form id.
   *
   * @test
   */
  public function formId() {
    $configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $form = new ConfigForm($configFactory->reveal());

    $this->assertEquals('lod_config', $form->getFormId());
  }

  /**
   * Form structure contains default values from config.
   *
   * @test
   */
  public function formStructureContainsDefaultValuesFromConfig() {
    $stringTranslation = $this->getStringTranslationStub();

    $config = $this->prophesize(Config::class);
    $config->get('endpoint')->willReturn('https://test.me/v1/');
    $config->get('user_key')->willReturn('ApiKeyTest');
    $configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $configFactory->getEditable('lod.config')->willReturn($config->reveal());

    $form = new ConfigForm($configFactory->reveal());
    $form->setStringTranslation($stringTranslation);
    $messenger = $this->prophesize(MessengerInterface::class)->reveal();
    $form->setMessenger($messenger);

    $formStructure = [];
    $formState = $this->prophesize(FormStateInterface::class);
    $structure = $form->buildForm($formStructure, $formState->reveal());

    $this->assertEquals(
      [
        '#type' => 'textfield',
        '#title' => new TranslatableMarkup('Endpoint URL', [], [], $stringTranslation),
        '#description' => new TranslatableMarkup('Provide the endpoint URL including the API version number.', [], [], $stringTranslation),
        '#default_value' => 'https://test.me/v1/',
        '#required' => TRUE,
      ],
      $structure['endpoint']
    );

    $this->assertEquals(
      [
        '#type' => 'textfield',
        '#title' => new TranslatableMarkup('API user key', [], [], $stringTranslation),
        '#description' => new TranslatableMarkup('Provide the (optional) API user key to access the service.', [], [], $stringTranslation),
        '#default_value' => 'ApiKeyTest',
        '#required' => FALSE,
      ],
      $structure['user_key']
    );
  }

  /**
   * Endpoint error if no valid URL.
   *
   * @test
   */
  public function endpointErrorWhenNotValidUrl() {
    $stringTranslation = $this->getStringTranslationStub();

    $formStructure = [];
    $formState = $this->createMock(FormStateInterface::class);
    $formState
      ->method('getValue')
      ->with($this->equalTo('endpoint'))
      ->willReturn('foo');
    $formState
      ->expects($this->once())
      ->method('setErrorByName')
      ->with(
        $this->equalTo('endpoint'),
        $this->equalTo(new TranslatableMarkup('Provide an absolute URL.', [], [], $stringTranslation))
      );

    $configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $form = new ConfigForm($configFactory->reveal());
    $form->setStringTranslation($stringTranslation);

    $form->validateForm($formStructure, $formState);
  }

  /**
   * Endpoint URL is formatted during validation.
   *
   * @test
   */
  public function endpointUrlIsFormattedWhileValidated() {

    $formStructure = [];
    $formState = $this->createMock(FormStateInterface::class);
    $formState
      ->method('getValue')
      ->with($this->equalTo('endpoint'))
      ->willReturn('http://test/me/v1');
    $formState
      ->expects($this->once())
      ->method('setValue')
      ->with(
        $this->equalTo('endpoint'),
        $this->equalTo('http://test/me/v1/')
      );

    $configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $form = new ConfigForm($configFactory->reveal());

    $form->validateForm($formStructure, $formState);
  }

  /**
   * Form values are stored in config.
   *
   * @test
   */
  public function formValuesAreStoredInConfiguration() {
    $stringTranslation = $this->getStringTranslationStub();

    $config = $this->prophesize(Config::class);
    $config->set('endpoint', 'https://test.me/v1/');
    $config->set('user_key', 'ApiKeyTest');
    $configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $configFactory->getEditable('lod.config')->willReturn($config->reveal());

    $formStructure = [];
    $formState = $this->createMock(FormStateInterface::class);
    $formState
      ->expects($this->at(0))
      ->method('getValue')
      ->with($this->equalTo('endpoint'))
      ->willReturn('https://test.me/v1/');
    $formState
      ->expects($this->at(1))
      ->method('getValue')
      ->with($this->equalTo('user_key'))
      ->willReturn('ApiKeyTest');

    $form = new ConfigForm($configFactory->reveal());
    $messenger = $this->prophesize(MessengerInterface::class)->reveal();
    $form->setStringTranslation($stringTranslation);
    $form->setMessenger($messenger);

    $form->submitForm($formStructure, $formState);
  }

}
